export class Patient{

	user_name:string;
        email:string;
        password:string;
        country:number;
        region:number;
        city:number;
        address:string;
        type_pain:string;
        level_pain:string;
        years_pain:number;
        usage_painkillers:boolean;
        painkiller:number;
        other_painkiller:string;
        public:boolean;
        history:string;
        step:number;
        fkcity:number;
        fkregion:number;
        fkcountry:number;
        patient_painstorypublic:boolean;
        patient_painyears:number;
        patient_otherpainkillers:string;
	fkpaintype:string;
	fkpainlevel:string;
	patient_usagepainkiller:string;
	patient_painstory:string;
	patient_username:string;
	patient_image:string;

	constructor(){}
}
