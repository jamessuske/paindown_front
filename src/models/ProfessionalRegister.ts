export class Professional{

        step:number;
	professional_username:string;
	professional_name:string;
        email:string;
        password:string;
        fkcity:number;
        fkregion:number;
        fkcountry:number;
        professional_address:string;
        professional_phone:string;
	fkprofession:string;
	professional_website:string;
	professional_about:string;
	professional_approach:string;
	professional_stories:string;
	professional_image:string;
	fkspecialities:Array<{id: number, text: string}>;
	fkqualifications:Array<{id: number, text: string}>;

	constructor(){}
}
