export class User{

	user_name:string;
	location = {
		country: {id: '', name: ''},
		city: {id: '', name: ''},
		region: {id: '', name: ''}
	}
	register:string;
	photo:string;
	cover:string;
	isPainkillers:boolean;
	usage_painkiller:number;
	other_painkiller:string;
	public:any;
	image:string;
	website:string;
	review_order:string;
	about:string;
	approach:string;
	stories:string;
	professional_image:string;
	patient_image:string;
	
	firstname:string;
	lastname:string;
	email:string;
	phone:string;
	news:boolean;
	sms:boolean;
	state:string;
	address:string;
        story:string;
        pain_type:any;
        pain_years:any;
        membership:any;
        profession:any;
	specialities:Array<{id: number, name: string}>;
	qualifications:Array<{id: number, name: string}>;
	preference:Array<{pkpreference: number, preference_name: string, patientpreference_selected: number }>;

	constructor(){

	}
}
