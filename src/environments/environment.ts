// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB-HNsmjdjWu5MN5fAzP6id1kIVWh4dH74',
    authDomain: 'paindown-770af.firebaseapp.com',
    databaseURL: 'https://paindown-770af.firebaseio.com',
    projectId: 'paindown-770af',
    storageBucket: 'paindown-770af.appspot.com',
    messagingSenderId: '357958398702'
  }
};
