import { Injectable } from '@angular/core';

import * as globals from '../app/globals';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import { UtilService } from './util.service';

@Injectable()

export class RegisterService {

	constructor(private http:Http, private utilService: UtilService){

	}

	/*guardar datos paso uno registro tradesman*/
	register(data){
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.post(globals.baseUrl+'/register', JSON.stringify(data) , options).toPromise().then(response => this.utilService.mapPost(response) ).catch( response=> '');
	}
	
	registerProfessional(data){
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.post(globals.baseUrl+'/registerProfessional', JSON.stringify(data) , options).toPromise().then(response => this.utilService.mapPost(response) ).catch( response=> '');
	}

	/*guardar datos paso dos registro tradesman*/
	registerTrades(data){
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.post(globals.baseUrl+'/registertrades', JSON.stringify(data) , options).toPromise().then(response => this.utilService.mapPost(response) ).catch( response=> '');
	}

	/*guardar datos paso tres registro tradesman*/ 
	businessTrades(data){
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.post(globals.baseUrl+'/businesstrades', JSON.stringify(data) , options).toPromise().then(response => this.utilService.mapPost(response) ).catch( response=> '');
	}

	/*guardar datos paso cinco registro tradesman*/
	companyTrades(data){
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.post(globals.baseUrl+'/companytrades', JSON.stringify(data) , options).toPromise().then(response => this.utilService.mapPost(response) ).catch( response=> '');
	}

	/*traer datos de categorias y certificacion*/
	types_accreditation():Observable<any>{
		return this.http.get(globals.baseUrl+'/typeregister', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

}
