import { Injectable } from '@angular/core';
import {Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { BlockUI, NgBlockUI } from 'ng-block-ui';
import swal from 'sweetalert2';
import {IMyDpOptions} from 'mydatepicker';


@Injectable()
export class UtilService{

	@BlockUI() blockUI: NgBlockUI;
	
	myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'dd-mm-yyyy',
        inline: false,
        editableDateField:false,
        openSelectorOnInputClick:true
    };
    
	images = [];

	constructor(){

	}

	blockUiStart(){
		this.blockUI.start();
	}
	
	blockUiStop(){
	    this.blockUI.stop();
	}

	alertError(message){
		swal('Oops...', message, 'error');
	}

	alertSuccess(message){
		swal('Complete', message, 'success')
	}

	getHeaderUrlencoded() {
		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		headers.append('Authorization',  'Bearer ' + localStorage.getItem('token'));
		return headers;
	}	

	getHeadersJson() {
		let headers = new Headers();
		headers.append('Accept', 'application/json');
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization',  'Bearer ' + localStorage.getItem('token'));
		return headers;
	}	

	map(response: Response): any {
		return response.json();
	}

	mapUsername(response: Response): any {
		return response;
	}

	mapPost(response: Response): any {
		if (response['_body']) 
			return response.json();
		return response;
	}

	
}

