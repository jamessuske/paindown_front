import { Injectable } from '@angular/core';

import * as globals from '../app/globals';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import { UtilService } from './util.service';

@Injectable()
export class PostJobService {

	constructor(private http:Http, private utilService: UtilService) {

	}

	category():Observable<any> {
		return this.http.get(globals.baseUrl+'/post-job', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	city(data):Observable<any>{
		return this.http.get(globals.baseUrl+'/city/'+data, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	addJob(data){
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.post(globals.baseUrl+'/post-job', JSON.stringify(data) , options).toPromise().then(response => this.utilService.mapPost(response) ).catch( response=> '');
	}

	listjob(data):Observable<any>{
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.get(globals.baseUrl+'/jobs/listjob', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

}
