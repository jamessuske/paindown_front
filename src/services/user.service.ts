import { Injectable } from '@angular/core';

import * as globals from '../app/globals';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import { UtilService } from './util.service';

@Injectable()
export class UserService{

	constructor(private http:Http, private utilService: UtilService){

	}
	
	getMessagesBySubject(subject):Observable<any> {
		return this.http.get(globals.baseUrl+'/messagesBySubject/' + subject, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	
	getMessages():Observable<any> {
	    return this.http.get(globals.baseUrl+'/messages', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getSentMessages():Observable<any> {
	    return this.http.get(globals.baseUrl+'/messages-sent', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getDeletedMessages():Observable<any> {
	    return this.http.get(globals.baseUrl+'/messages-deleted', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getMessage(id):Observable<any> {
		return this.http.get(globals.baseUrl+'/message/' + id, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	
	composeMessage(id):Observable<any> {
		return this.http.get(globals.baseUrl+'/composeMessage/' + id, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	
	postMessage(user) {
		let options = new RequestOptions({headers: this.utilService.getHeaderUrlencoded()});
		return this.http.post(globals.baseUrl+'/postMessage', `recipients=${user.recipients}&subject=${user.subject}&message=${user.body}`, options).toPromise();
	}
	
	updateMessage(user) {
		let options = new RequestOptions({headers: this.utilService.getHeaderUrlencoded()});
		return this.http.put(globals.baseUrl+'/updateMessage', `id=${user.id}&message=${user.body}`, options).toPromise();
	}
	
	deleteMessage(user) {
		let options = new RequestOptions({headers: this.utilService.getHeaderUrlencoded()});
		return this.http.put(globals.baseUrl+'/deleteMessage', `id=${user}`, options).toPromise();
	}

	getBlogPosts():Observable<any> {
		return this.http.get('/paindown_blog/wp-json/wp/v2/posts', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getBlogPost(postid):Observable<any> {
		return this.http.get('/paindown_blog/wp-json/wp/v2/posts/' + postid, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getUserLocation():Observable<any> {
		return this.http.get('https://api.ipdata.co?api-key=84566edfceec530ccea2010c432a7a2aea752699ab9c6b87559102d9').map(this.utilService.map);
	}

	login(user) {
		let options = new RequestOptions({headers: this.utilService.getHeaderUrlencoded()});
	    return this.http.post(globals.baseUrl+'/login', `email=${user.email}&password=${user.password}`, options).toPromise();
	}

	review(user) {

		let options = new RequestOptions({headers: this.utilService.getHeaderUrlencoded()});
		return this.http.post(globals.baseUrl+'/reviews', `problem=${user.problem}&text=${user.text}&rating=${user.rating}&user=${user.user}`, options).toPromise();

	}
	
	forgot(user) {
		let options = new RequestOptions({headers: this.utilService.getHeaderUrlencoded()});
	    return this.http.post(globals.baseUrl+'/forgot', `email=${user.email}`, options).toPromise();
	}
	
	resetPassword(user) {
		let options = new RequestOptions({headers: this.utilService.getHeaderUrlencoded()});
		return this.http.post(globals.baseUrl+'/reset-password', `email=${user.email}&token=${user.token}&password=${user.password}&password_confirmation=${user.repeat}`, options).toPromise();
	}

	profile():Observable<any> {
	    return this.http.get(globals.baseUrl+'/profile', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	update(data){
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.put(globals.baseUrl+'/user/0', JSON.stringify(data) , options).toPromise().then(response => this.utilService.mapPost(response) ).catch( response=> '');
	}

	updatePassword(data) {
	
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.put(globals.baseUrl+'/updatePassword', JSON.stringify(data) , options).toPromise().then(response => this.utilService.mapPost(response) ).catch( response=> '');
		
	}
	updatePerference(data) {
	
		let options = new RequestOptions({headers: this.utilService.getHeadersJson()});
		return this.http.put(globals.baseUrl+'/updatePerference', JSON.stringify(data) , options).toPromise().then(response => this.utilService.mapPost(response) ).catch( response=> '');
		
	}

	getProfileByUsername(username, order):Observable<any> {
		return this.http.get(globals.baseUrl+'/getUserByUsername/' + username + '/' + order, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getAllProfessionals():Observable<any> {
	    return this.http.get(globals.baseUrl+'/professionals', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	getAllProfessionalsMap(long, lat):Observable<any> {
		return this.http.get(globals.baseUrl+'/professionals/' + long + '/' + lat, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	
	getProfessionalsByUsername(username):Observable<any> {
            return this.http.get(globals.baseUrl+'/professionals/' + username, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
        }
	
	getProfessionals(pros, spec, location, order, disMin, disMax, long, lat):Observable<any> {
		return this.http.get(globals.baseUrl+'/professionalsFilters/' + pros + '/' + spec + '/' + location + '/' + order + '/' + disMin + '/' + disMax + '/' + long + '/' + lat, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	
	getAllPatients():Observable<any> {
	    return this.http.get(globals.baseUrl+'/patients', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	getAllPatientsMap(long, lat):Observable<any> {
		return this.http.get(globals.baseUrl+'/patients/' + long + '/' + lat, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	
	getPatientsByUsername(username):Observable<any> {
            return this.http.get(globals.baseUrl+'/patients/' + username, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
        }
	
	getPatients(painType, location, order, disMin, disMax, painMin, painMax, painKillers, long, lat):Observable<any> {
		return this.http.get(globals.baseUrl+'/patientsFilters/' + painType + '/' + location + '/' + order + '/' + disMin + '/' + disMax + '/' + painMin + '/' + painMax + '/' + painKillers + '/' + long + '/' + lat, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}
	
	getMapLocationPatients(location):Observable<any> {
            return this.http.get(globals.baseUrl+'/patientsMap/' + location, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
        }
	
	getMapLocationProfessionals(location):Observable<any> {
            return this.http.get(globals.baseUrl+'/professionalsMap/' + location, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
        }
	

}
