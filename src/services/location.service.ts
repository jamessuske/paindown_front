import { Injectable } from '@angular/core';

import * as globals from '../app/globals';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import { UtilService } from './util.service';

@Injectable()
export class LocationService{

	constructor(private http:Http, private utilService: UtilService){}

	getCountrys(filter):Observable<any> {
	    return this.http.get(globals.baseUrl+'/country/' + filter, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getRegions(filter):Observable<any> {
	    return this.http.get(globals.baseUrl+'/region/' + filter, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getCities(filter):Observable<any> {
	    return this.http.get(globals.baseUrl+'/city/' + filter, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

}