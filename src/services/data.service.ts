import { Injectable } from '@angular/core';

import * as globals from '../app/globals';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { map } from 'rxjs/operators';

import { UtilService } from './util.service';

@Injectable()
export class DataService{

	constructor(private http:Http, private utilService: UtilService){}

	checkUsername(username):Observable<any> {
	    return this.http.get(globals.baseUrl+'/user/' + username, {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	/*checkUsername(username):Observable<any> {
		return this.http.get(globals.baseUrl+'/user/' + username, {headers:this.utilService.getHeadersJson()});
	}*/

	getPainTypes():Observable<any> {
	    return this.http.get(globals.baseUrl+'/paintype', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getPainLevel():Observable<any> {
	    return this.http.get(globals.baseUrl+'/painlevel', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getPainKiller():Observable<any> {
	    return this.http.get(globals.baseUrl+'/painkiller', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getProfessions():Observable<any> {
	    return this.http.get(globals.baseUrl+'/profession', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getSpecialities():Observable<any> {
	    return this.http.get(globals.baseUrl+'/specialities', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

	getQualification():Observable<any> {
	    return this.http.get(globals.baseUrl+'/qualification', {headers:this.utilService.getHeadersJson()}).map(this.utilService.map);
	}

}
