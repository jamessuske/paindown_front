import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import * as firebase from 'firebase';
import * as globals from '../app/globals'
 
import {FileUpload} from '../models/FileUpload';
import { UtilService } from './util.service'
 
@Injectable()
export class UploadFileService {
 
  constructor(private db: AngularFireDatabase, private  utilService: UtilService) {}
 
  private basePath = globals.storageUrl + localStorage.getItem('id');
 
  pushFileToStorage(fileUpload: FileUpload, progress: {percentage: number}) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${fileUpload.file.name}`).put(fileUpload.file);
 
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        // in progress
        const snap = snapshot as firebase.storage.UploadTaskSnapshot
        progress.percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100)
      },
      (error) => {
        // fail
        console.log(error)
      },
      () => {
        // success
        fileUpload.url = uploadTask.snapshot.downloadURL
        fileUpload.name = fileUpload.file.name
        this.saveFileData(fileUpload)
        this.utilService.images.push(fileUpload.url);
      }
    );
  }
 
  saveFileData(fileUpload: FileUpload) {
    this.db.list(`${this.basePath}/`).push(fileUpload);
  }
}