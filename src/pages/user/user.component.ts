import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UtilService } from '../../services/util.service';

@Component({
  selector: 'app-user-public',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private route: ActivatedRoute, private userService: UserService,
  private utilService: UtilService) { }

  tab = '0';
  username = '';
  user;
  type = 0;

  ngOnInit() {
    this.utilService.blockUiStart();
    this.route.params.subscribe(params => {
      this.username = params.username;
      this.userService.getProfileByUsername(this.username, "0").subscribe(data => {
        this.user = data;
        this.type = data.membership;
        this.utilService.blockUiStop();
      });
    });
  }

}
