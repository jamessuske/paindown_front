import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';

import { UserService } from '../../services/user.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-messages-deleted',
  templateUrl: './messages-deleted.component.html',
  styleUrls: ['./messages-deleted.component.css']
})

export class MessagesDeletedComponent implements OnInit {

  userMessages = [];
  textSearch: String;

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router, private utilService: UtilService ) {

    this.textSearch = "";

    title.setTitle('.:Paindown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
	this.utilService.blockUiStart();
                this.userService.getDeletedMessages().subscribe(data => {
			this.userMessages = data;
                        this.utilService.blockUiStop();
                });
  }


  onSubmit($value){
                this.utilService.blockUiStart();
                this.userService.getMessagesBySubject(this.textSearch).subscribe(data => {
                        this.userMessages = data;
                        this.utilService.blockUiStop();
                });
  }

	openNav() {
                $('.sidebar').addClass('expanded');
        }
        closeNav() {
                $('.sidebar').removeClass('expanded');
        }
	mobileSearch() {
                $('body').addClass('modal-open');
                $('#searchModal').addClass('in');
                $('#searchModal').css('display', 'block');
                $('body').append("<div class='modal-backdrop fade in'></div>");

        }
        mobileCloseSearch() {

                $('body').removeClass('modal-open');
                $('#searchModal').removeClass('in');
                $('#searchModal').css('display', 'none');
                $('.modal-backdrop').remove();


        }

}
