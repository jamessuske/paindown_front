import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';

import { UserService } from '../../services/user.service'
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})

export class ResetPasswordComponent implements OnInit {

  user = { token: '', email: '', password: '', repeat: '' }
  error:boolean = false;

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router, private route: ActivatedRoute,  private utilService: UtilService ) {

    title.setTitle('.:Paindown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
  	this.utilService.blockUiStart();
	this.route.params.subscribe(params => {
		this.user.token = params.token;
		this.user.email = params.email;
		this.utilService.blockUiStop();
	});
  }

  login(){
  	this.utilService.blockUiStart();
	console.log(this.user);
	this.userService.resetPassword(this.user).then(data=>{
		let response = this.utilService.mapPost(data);
		this.utilService.blockUiStop();
		this.utilService.alertSuccess(response.response);
		this.router.navigateByUrl('login');
	 }).catch(error=>{
	 	this.utilService.blockUiStop();
		this.utilService.alertError('The email address enter is not assciated with an account.');
		this.error = true;
	 });
  }

}
