import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';
import { UserService } from '../../services/user.service';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import { CompleterService, CompleterData } from 'ng2-completer';
import {Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public categories;
  public projects;
	protected searchStr: string;
	protected patsearchStr: string;
	long: String;
	lat: String;
  	step1Form: FormGroup;
  	step2Form: FormGroup;
  	step3Form: FormGroup;
  	activeForm;
	tabActive:number;
	tabSubActive:number;
	patstep1Form: FormGroup;
  	patstep2Form: FormGroup;
  	patstep3Form: FormGroup;
	patstep4Form: FormGroup;
  	patactiveForm;
	pattabActive:number;
	pattabSubActive:number;
  posts = [];
  proItems = [];
  patItems = [];
	painType: number;
  location: number;
	pros: number;
	distanceRange: number[] = [15,45];
	painRanges: number[] = [10, 20];
	painkillers: string;
	minRange: number;
	maxRange: number;
	patlocation: number;
	patpros: number;
	patdistanceRange: number[] = [15,45];
	patminRange: number;
	patmaxRange: number;
	spec: number;
	professions = [];
        specialities = [];
	painTypes = [];
	distanceRangeApply:boolean = false;
	selectedItem: any = '';
  inputChanged: any = '';
	patselectedItem: any = '';
  patinputChanged: any = '';
  proConfig: any = {'placeholder': 'Pain or illness', 'sourceField': ['text']};
  patConfig: any = {'placeholder': 'Pain or illness', 'sourceField': ['name']};

  constructor(private completerService: CompleterService, private fb: FormBuilder, meta: Meta, title: Title, private userService: UserService, private dataService: DataService, private router:Router,  private utilService: UtilService) {

	  this.tabActive = 0;
		this.tabSubActive = 0;
	  this.location = 0;
	  this.pattabActive = 0;
		this.pattabSubActive = 0;
	  this.patlocation = 0;
	  this.pros = 0;
	  this.spec = 0;
	  this.painType  = 0;
	  this.painkillers = "Yes";
	  this.long = "";
	  this.lat = "";
	  
    title.setTitle('.:PainDown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
    this.buildForm();
    this.buildFormPat();
    this.utilService.blockUiStart();
    this.userService.getBlogPosts().subscribe(data => {
            this.posts = data;
            this.utilService.blockUiStop();
    });
    this.dataService.getSpecialities().subscribe(data => {
    	this.proItems = data.map(a => a.text);
	this.dataService.getPainTypes().subscribe(data => {
            this.patItems = data.map(a => a.name);
            this.dataService.getProfessions().subscribe(data => {
                this.professions = data;
            });
	    this.dataService.getSpecialities().subscribe(data => {
                this.specialities = data;
            });
            this.dataService.getPainTypes().subscribe(data => {
                this.painTypes = data;
	    });
	    this.userService.getUserLocation().subscribe(data => {
	    	this.lat = data.latitude;
                this.long = data.longitude;
            });
        });

    });


}
	

	
	buildForm(): void {
		
		this.step1Form = this.fb.group({
        	location: new FormControl('', {
            }),
			distanceRange: new FormControl('', {
            }),
		});
		this.step2Form = this.fb.group({
        	pros: new FormControl('', {
            }),
		});
		this.step3Form = this.fb.group({
        	spec: new FormControl('', {
            }),
		});
		this.activeForm = this.step1Form;
		
	}
	
	buildFormPat(): void {
		
		this.patstep1Form = this.fb.group({
        	patlocation: new FormControl('', {
            }),
			patdistanceRange: new FormControl('', {
            }),
		});
		this.patstep2Form = this.fb.group({
        	painType: new FormControl('', {
            }),
		});
		this.patstep3Form = this.fb.group({
        	painkillers: new FormControl('', {
            }),
		});
		this.patstep4Form = this.fb.group({
        	painRanges: new FormControl('', {
            }),
		});
		this.patactiveForm = this.patstep1Form;
		
	}
	
	next(){
		this.getActiveForm();
		if(this.activeForm.valid){
			if(this.tabActive == 2){
				
				this.router.navigateByUrl('results/professionals/0/' + this.location + '/' + this.long + '/' + this.lat + '/' + this.distanceRange[0] + '/' + this.distanceRange[1] + '/0/0/0/0/' + this.pros + '/' + this.spec);
				
		}else{
				this.tabActive = this.tabActive + 1;
				this.setActiveForm();
			}	
		}
	}

	back(){
		this.tabActive = this.tabActive - 1;
		this.getActiveForm();	
	}

	getActiveForm(){
		switch(this.tabActive){
			case 0:
				this.activeForm = this.step1Form;
			break;
			case 1:
				this.activeForm = this.step2Form;
			break;
			case 2:
				this.activeForm = this.step3Form;
			break;
		}
	}

	setActiveForm(){
		switch(this.tabActive){
			case 0:
				this.activeForm = this.step1Form;
			break;
			case 1:
				this.activeForm = this.step2Form;
			break;
			case 2:
				this.activeForm = this.step3Form;
			break;
		}
	}

	changeSubTab(subTab:number){
		this.tabSubActive = subTab;
	}
	
	patnext(){
		this.patgetActiveForm();
		if(this.patactiveForm.valid){
			if(this.pattabActive == 3){
				
				console.log(this.painType);
				console.log(this.painkillers);
				console.log(this.patlocation);
				console.log(this.patdistanceRange);
				console.log(this.painRanges);
				
				this.router.navigateByUrl('results/patients/0/' + this.patlocation + '/' + this.long + '/' + this.lat + '/' + this.patdistanceRange[0] + '/' + this.patdistanceRange[1] + '/' + this.painRanges[0] + '/' + this.painRanges[1] + '/' + this.painType + '/' + this.painkillers + '/0/0');
				
			}else{
				this.pattabActive = this.pattabActive + 1;
				this.patsetActiveForm();
			}	
		}
	}

	patback(){
		this.pattabActive = this.pattabActive - 1;
		this.patgetActiveForm();	
	}

	patgetActiveForm(){
		switch(this.pattabActive){
			case 0:
				this.patactiveForm = this.patstep1Form;
			break;
			case 1:
				this.patactiveForm = this.patstep2Form;
			break;
			case 2:
				this.patactiveForm = this.patstep3Form;
			break;
			case 3:
				this.patactiveForm = this.patstep4Form;
			break;
		}
	}

	patsetActiveForm(){
		switch(this.pattabActive){
			case 0:
				this.patactiveForm = this.patstep1Form;
			break;
			case 1:
				this.patactiveForm = this.patstep2Form;
			break;
			case 2:
				this.patactiveForm = this.patstep3Form;
			break;
			case 3:
				this.patactiveForm = this.patstep4Form;
			break;
		}
	}

	patchangeSubTab(subTab:number){
		this.pattabSubActive = subTab;
	}
	
	ProSearch() {
                $('body').addClass('modal-open');
                $('#proModal').addClass('in');
                $('#proModal').css('display', 'block');
                $('body').append("<div class='modal-backdrop fade in'></div>");

    }
	ProCloseSearch() {

                $('body').removeClass('modal-open');
                $('#proModal').removeClass('in');
                $('#proModal').css('display', 'none');
                $('.modal-backdrop').remove();


    }
	
	PatSearch() {
                $('body').addClass('modal-open');
                $('#patModal').addClass('in');
                $('#patModal').css('display', 'block');
                $('body').append("<div class='modal-backdrop fade in'></div>");

    }
	PatCloseSearch() {

                $('body').removeClass('modal-open');
                $('#patModal').removeClass('in');
                $('#patModal').css('display', 'none');
                $('.modal-backdrop').remove();


    }
	
	onSelect(item: any) {
    this.selectedItem = item;
  }
 
  onInputChangedEvent(val: string) {
    this.inputChanged = val;
  }
	
	patonSelect(item: any) {
    this.patselectedItem = item;
  }
 
  patonInputChangedEvent(val: string) {
    this.patinputChanged = val;
  }

}
