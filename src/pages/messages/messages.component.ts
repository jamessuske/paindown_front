import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';

import { UserService } from '../../services/user.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})

export class MessagesComponent implements OnInit {

  userMessages = [];
  selectedMessages = [];
  textSearch: String;

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router, private utilService: UtilService ) {

    this.textSearch = "";

    title.setTitle('.:Paindown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
	this.utilService.blockUiStart();
                this.userService.getMessages().subscribe(data => {
			this.userMessages = data;
                        this.utilService.blockUiStop();
                });
  }

	openNav() {
                $('.sidebar').addClass('expanded');
        }
        closeNav() {
                $('.sidebar').removeClass('expanded');
        }
	mobileSearch() {
                $('body').addClass('modal-open');
                $('#searchModal').addClass('in');
                $('#searchModal').css('display', 'block');
                $('body').append("<div class='modal-backdrop fade in'></div>");

        }
        mobileCloseSearch() {

                $('body').removeClass('modal-open');
                $('#searchModal').removeClass('in');
                $('#searchModal').css('display', 'none');
                $('.modal-backdrop').remove();


        }


  messageSelected($event, index) {
        if($event.target.checked){
                this.selectedMessages.push($event.target.getAttribute('name'));
        }
        else
        {
                var item = this.selectedMessages.indexOf($event.target.getAttribute('name'), 0);
                this.selectedMessages.splice(item, 1);
        }
        console.log(this.selectedMessages);
  }

  onSubmit($value){
                this.utilService.blockUiStart();
                this.userService.getMessagesBySubject(this.textSearch).subscribe(data => {
                        this.userMessages = data;
                        this.utilService.blockUiStop();
                });
  }

  deleteMessages() {
        this.utilService.blockUiStart();
        for (let message of this.selectedMessages){
                this.userService.deleteMessage(message).then(data=>{
                        let response = this.utilService.mapPost(data);
                        console.log(response);
                });
        }
        this.userService.getSentMessages().subscribe(data => {
                this.userMessages = data;
                this.utilService.blockUiStop();
        });
  }

}
