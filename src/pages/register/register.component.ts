import { Component, OnInit } from '@angular/core';
import * as globals from '../../app/globals';
import { Router } from '@angular/router';
import {Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';
import { MessageBag, ValidationMessagesService } from 'ng2-custom-validation';
import { map } from 'rxjs/operators';

import {AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { UUID } from 'angular2-uuid';
import * as firebase from 'firebase/app'

/*SERVICES*/
import { UtilService } from '../../services/util.service';
import { RegisterService } from '../../services/register.service';
import { LocationService } from '../../services/location.service';
import { DataService } from '../../services/data.service';

/*MODELS*/
import { City } from '../../models/City';
import { Patient } from '../../models/PatientRegister';
import { FileUpload } from '../../models/FileUpload';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styles: []
})
export class RegisterComponent{

	errors: MessageBag = new MessageBag();
	step1Form: FormGroup;
	step2Form: FormGroup;
	step3Form: FormGroup;
	step4Form: FormGroup;
        step5Form: FormGroup;
	activeForm;
	city = {id: '', value: ''};
	countries = {};
	regions = {};
	cities = {};
	painTypes = {};
	painLevels = {};
	painKillers = {};
	usernames = [];
	source = globals.baseUrl+'/city/:my_own_keyword';

	patient:Patient;
	tabActive:number;
	tabSubActive:number;
	subtypes;
	username_already:boolean = false;


	items: AngularFireList<any[]>;
        selectedFiles: FileList
        currentFileUpload: FileUpload;
        progress: number = 0;
        //basePath = globals.storageUrl + localStorage.getItem('id');
        basePath = globals.storageUrl;
        images = [];
        uploading: boolean = false;

	constructor(
		private utilService:UtilService,
		private registerService:RegisterService,
		private router:Router,
		private fb: FormBuilder,
		private locationService: LocationService,
		private dataService: DataService,
        private validationMessagesService: ValidationMessagesService
	) {
		this.patient = new Patient;
		this.patient.step = 0;
		this.patient.fkpaintype = "0";
		this.patient.fkpainlevel = "0";
		this.patient.patient_usagepainkiller = "0";
		this.patient.patient_image = "";
		this.patient.patient_painstorypublic = false;
                this.patient.patient_painyears = 5;
                this.patient.patient_otherpainkillers = "";
		this.tabActive = 0;
		this.tabSubActive = 0;
		this.patient.patient_painstory = "<p>Sed varius nisl id malesuada rutrum. Praesent sodales mattis rhoncus. Cras in sem a est euismod maxim us nec sed dui. Curabitur id nisl condimentum, sodales mauris eget, tempus felis.</p><p>Aenean in porttitor libero, vitae euismod dolor. Aliquam vel sagittis est, vitae volutpat nulla. Inte ger cursus sagittis erat at rhoncus. Sed blandit feugiat imperdiet. Nunc aliquam ut nunc eget accumsan. Nunc ut nisi eget augue aliquet porta a vitae justo.</p><p>Sed varius nisl id malesuada rutrum. Praesent sodales mattis rhoncus. Cras in sem a est euismod maxim us nec sed dui. Curabitur id nisl condimentum, sodales mauris eget, tempus felis.</p><p>Aenean in porttitor libero, vitae euismod dolor. Aliquam vel sagittis est, vitae volutpat nulla. Inte ger cursus sagittis erat at rhoncus. Sed blandit feugiat imperdiet. Nunc aliquam ut nunc eget accumsan. Nunc ut nisi eget augue aliquet porta a vitae justo.";
		this.patient.patient_username = "";
	}

	ngOnInit(): void {
		this.buildForm();
		this.locationService.getCountrys('all').subscribe(data => this.countries = data);
		this.dataService.getPainTypes().subscribe(data => this.painTypes = data);
		this.dataService.getPainLevel().subscribe(data => this.painLevels = data);
		this.dataService.getPainKiller().subscribe(data => this.painKillers = data);
		var config = {
                        apiKey: "AIzaSyB-HNsmjdjWu5MN5fAzP6id1kIVWh4dH74",
                        authDomain: "paindown-770af.firebaseapp.com",
                        databaseURL: "https://paindown-770af.firebaseio.com",
                        projectId: "paindown-770af",
                        storageBucket: "paindown-770af.appspot.com",
                        messagingSenderId: "357958398702"
                };
		if (!firebase.apps.length) {
			firebase.initializeApp(config);
		}

	}
	
	buildForm(): void {
		
		this.step1Form = this.fb.group({
		username: new FormControl('', {
                        validators: [Validators.required,
			Validators.minLength(4)],
		}),
                email: new FormControl('', {
                        validators: [Validators.required,
                        Validators.pattern("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")],
                }),
                password: new FormControl('', {
                        validators: [Validators.required,
                        Validators.minLength(8)],
                }),
		repeat: new FormControl('', {
                        validators: [Validators.required,
                        Validators.minLength(8)],
		}),
		}, {validator: this.passwordMatchValidator, asyncValidator: this.checkUsername.bind(this)});
        	this.step2Form = this.fb.group({
                	country: new FormControl('', {
                	}),
                	region: new FormControl('', {
                	}),
                	city: new FormControl('', {
                	}),
                	address: new FormControl('', {
                        	validators: [Validators.required]
                	}),
        	});
        	this.step3Form = this.fb.group({
                	painType: new FormControl('', {
				validators: [Validators.required]
                	}),
                	painLevel: new FormControl('', {
				validators: [Validators.required]
                	}),
                	painYears: new FormControl('', {
                	})
			}, { validator: this.painTypeValidator });
        	this.step4Form = this.fb.group({
                	painKiller: new FormControl('', {
                	}),
                	painKillerOther: new FormControl('', {
                	})
        	});
        	this.step5Form = this.fb.group({
                	painStoryPublic: new FormControl('', {
                	}),
                	painStory: new FormControl('', {
                	})
        	});

		this.activeForm = this.step1Form;
        	this.validationMessagesService.seeForErrors(this.step1Form).subscribe((errors: MessageBag) => {
			this.errors = errors;
		});
		this.validationMessagesService.seeForErrors(this.step2Form).subscribe((errors: MessageBag) => {
                	this.errors = errors;
        	});
	}


	profileImgClick(event) {
                $("#profile-image").trigger('click');
        }
        selectFile(event) {
                this.selectedFiles = event.target.files;
                this.upload();
        }
        upload() {
                let file = this.selectedFiles.item(0);
                let ext = '.' + file.name.split('.').pop();
                let uuid = UUID.UUID();
                var blob = file.slice(0, -1, file.type);
                var newFile = new File([blob], uuid + ext, {type: file.type});
                this.currentFileUpload = new FileUpload(newFile);
                this.pushFileToStorage(this.currentFileUpload);
        }
        pushFileToStorage(fileUpload: FileUpload) {
                this.uploading = true;
                const storageRef = firebase.storage().ref();
                const uploadTask = storageRef.child(`${this.basePath}/${fileUpload.file.name}`).put(fileUpload.file);
                uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                  (snapshot) => {
                        // in progress
                        const snap = snapshot as firebase.storage.UploadTaskSnapshot
                        this.progress = Math.round((snap.bytesTransferred / snap.totalBytes) * 100)
                  },
                  (error) => {
                        // fail
                        console.log(error)
                        this.utilService.alertError('There was an issue uploading your profile image, please try again.');
                        this.uploading = false;
                  },
                  () => {
                        // success
                        fileUpload.url = uploadTask.snapshot.downloadURL
                        fileUpload.name = fileUpload.file.name
                        //this.uploadService.saveFileData(fileUpload)
                        this.utilService.images.push(fileUpload.url);
                        this.patient.patient_image = fileUpload.url;
                        this.uploading = false;
                  }
                );
        }

	onSelectCountry($value){
		this.locationService.getRegions($value.value).subscribe(data => this.regions = data);
		this.patient.fkcountry = $value.data[0].id;
	}

	onSelectRegion($value){
		this.locationService.getCities($value.value).subscribe(data => this.cities = data);
		this.patient.fkregion = $value.data[0].id;
	}

	onSelectCity($value){
		this.patient.fkcity = $value.data[0].id;
	}

	passwordMatchValidator(g: FormGroup) {
			//this.dataService.checkUsername(g.get('username').value).subscribe(data => this.usernames = data);
			return g.get('password').value === g.get('repeat').value ? null : {'mismatch': true};

	}

	painTypeValidator(g: FormGroup) {

		return g.get('painType').value != "0" && g.get('painLevel').value != 0 ? null : {'mismatch' : true};

	}


	/*checkUsername(g: FormGroup) {

		return this.dataService.checkUsername(g.get('username').value).subscribe(data => {
			console.log(typeof data["error"] != 'undefined' ? null : {'mismatch': true});
			return typeof data["error"] != 'undefined' ? null : {'mismatch': true};
		});

	}*/


	checkUsername(g: FormGroup) {
		return this.dataService.checkUsername(g.get('username').value).pipe(map(data => {
			return typeof data["error"] != 'undefined' ? null : {'taken': true};
		}));
	}


	myListFormatter(data: any): string {
    		return `${data['value']}`;
    	}

	next(){
		this.getActiveForm();
		if(this.activeForm.valid){
			if(this.tabActive == 5)
				this.sendData();
			else{
				this.tabActive = this.tabActive + 1;
				this.setActiveForm();
			}	
		}
	}

	back(){
		this.tabActive = this.tabActive - 1;
		this.getActiveForm();	
	}

	getActiveForm(){
		switch(this.tabActive){
			case 0:
				this.activeForm = this.step1Form;
			break;
			case 1:
				this.activeForm = this.step2Form;
			break;
			case 2:
				this.activeForm = this.step3Form;
			break;
			case 3:
                                this.activeForm = this.step4Form;
                        break;
                        case 4:
                                this.activeForm = this.step5Form;
                        break;
		}
	}

	setActiveForm(){
		switch(this.tabActive){
			case 0:
				this.activeForm = this.step1Form;
			break;
			case 1:
				this.activeForm = this.step2Form;
			break;
			case 2:
				this.activeForm = this.step3Form;
			break;
			case 3:
                                this.activeForm = this.step4Form;
                        break;
                        case 4:
                                this.activeForm = this.step5Form;
                        break;
		}
	}

	changeSubTab(subTab:number){
		this.tabSubActive = subTab;
	}

	sendData(){
		this.utilService.blockUiStart();
                this.registerService.register(this.patient).then( data=>{
                        this.utilService.blockUiStop();
			console.log(data);
                        data.status == true ? this.utilService.alertSuccess(data.message) : this.utilService.alertError(data.message);
                });
	}
}
