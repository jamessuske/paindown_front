import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';

import { UserService } from '../../services/user.service'
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-thread-message',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.css']
})

export class ThreadComponent implements OnInit {

  userMessages = [];
  user = { id: 0, body: ''}
  subject = ''
  error:boolean = false;

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router, private route: ActivatedRoute, private utilService: UtilService ) {

    title.setTitle('.:Paindown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
      ]);

  }

  ngOnInit() {
  	this.utilService.blockUiStart();
	this.route.params.subscribe(params => {
		this.user.id = params.id;
                this.userService.getMessage(params.id).subscribe(data => {
			console.log(data);
			this.userMessages = data;
			this.subject = data.thread.subject;
                        this.utilService.blockUiStop();
		});

	});
  }


	openNav() {
                $('.sidebar').addClass('expanded');
        }
        closeNav() {
                $('.sidebar').removeClass('expanded');
        }

  send() {

	this.utilService.blockUiStart();
        this.userService.updateMessage(this.user).then(data=>{
                        let response = this.utilService.mapPost(data);
			this.user.body = "";
                	this.userService.getMessage(this.user.id).subscribe(data => {
				this.userMessages = data;
                        	this.utilService.blockUiStop();
			});
        }).catch(error=>{
                this.utilService.blockUiStop();
                this.utilService.alertError("Something went wrong, please try again later.");
                this.error = true;
        });

  }

}
