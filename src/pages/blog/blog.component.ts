import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})

export class BlogComponent implements OnInit {

  posts = [];

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router,  private utilService: UtilService ) {

    title.setTitle('.:Paindown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
  	this.utilService.blockUiStart();
	this.userService.getBlogPosts().subscribe(data => {
		this.posts = data;
		console.log(this.posts);
		this.utilService.blockUiStop();
	});
  }

}
