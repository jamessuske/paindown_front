import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';

import { UserService } from '../../services/user.service'
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  user = { subject: '', body: '', recipients: '' }
  error:boolean = false;
  session:boolean= false;

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router, private route: ActivatedRoute, private utilService: UtilService ) {

    title.setTitle('.:Paindown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
  	if (localStorage.getItem('token')) {
		this.session = true;
	}
	this.utilService.blockUiStart();
  	this.route.params.subscribe(params => {
		this.userService.composeMessage(params.username).subscribe(data => {
			console.log(data);
			this.user.recipients = data[0].username;
			this.utilService.blockUiStop();
		});
	});
  }

	openNav() {
                $('.sidebar').addClass('expanded');
        }
        closeNav() {
                $('.sidebar').removeClass('expanded');
        }

  send() {

	this.utilService.blockUiStart();
	this.userService.postMessage(this.user).then(data=>{
			let response = this.utilService.mapPost(data);
			this.utilService.blockUiStop();
			this.router.navigateByUrl('messages');
	}).catch(error=>{
        	this.utilService.blockUiStop();
                this.utilService.alertError("Something went wrong, please try again later.");
                this.error = true;
        });

  }

}
