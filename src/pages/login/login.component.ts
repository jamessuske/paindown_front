import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';

import { UserService } from '../../services/user.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-logn',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user={
    email: '',
    password: ''
  }

  error:boolean = false;

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router,  private utilService: UtilService ) {

    title.setTitle('.:Login:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
    if(localStorage.getItem('token'))
      this.router.navigateByUrl('account');
  }

  login(){
    this.utilService.blockUiStart();
    this.userService.login(this.user).then(data=>{
      let response = this.utilService.mapPost(data);
      localStorage.setItem('token', response.token);
      localStorage.setItem('state', response.user.state);
      this.utilService.blockUiStop();
      this.router.navigateByUrl('account');

    }).catch(error=>{
      this.utilService.blockUiStop();
      this.utilService.alertError('Your email or password are not correct or your account is not activated.');
      this.error = true;
    });
  }

}
