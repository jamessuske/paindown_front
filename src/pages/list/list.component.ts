import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  
  type = '';

  constructor(meta: Meta, title: Title, private router:Router, private route: ActivatedRoute) {

    title.setTitle('.:PainDown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.type = params.type;
    });
  }

}
