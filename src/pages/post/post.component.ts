import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})

export class PostComponent implements OnInit {

  posts = [];

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router, private route: ActivatedRoute, private utilService: UtilService ) {

    title.setTitle('.:Paindown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
  	this.utilService.blockUiStart();
	this.route.params.subscribe(params => {
		this.userService.getBlogPost(params.id).subscribe(data => {
			this.posts = data;
			console.log(this.posts);
			this.utilService.blockUiStop();
		});

	});
  }

}
