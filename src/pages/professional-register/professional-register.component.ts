import { Component, OnInit } from '@angular/core';
import * as globals from '../../app/globals';
import { Router } from '@angular/router';
import {Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';
import { MessageBag, ValidationMessagesService } from 'ng2-custom-validation';
import { map } from 'rxjs/operators';

import {AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { UUID } from 'angular2-uuid';
import * as firebase from 'firebase/app'

/*SERVICES*/
import { UtilService } from '../../services/util.service';
import { RegisterService } from '../../services/register.service';
import { LocationService } from '../../services/location.service';
import { DataService } from '../../services/data.service';

/*MODELS*/
import { City } from '../../models/City';
import { Professional } from '../../models/ProfessionalRegister';
import { FileUpload } from '../../models/FileUpload';

@Component({
	selector: 'app-professional-register',
	templateUrl: './professional-register.component.html',
	styles: []
})
export class ProfessionalRegisterComponent{

	errors: MessageBag = new MessageBag();
	step1Form: FormGroup;
	step2Form: FormGroup;
	step3Form: FormGroup;
	step4Form: FormGroup;
	step5Form: FormGroup;
	step6Form: FormGroup;
	step7Form: FormGroup;
	step8Form: FormGroup;
	activeForm;
	city = {};
	countries = {};
	regions = {};
	cities = {};
	professions = {};
	specialities = [];
	qualifications = [];
	selectedSpecialities = [];
	selectedQualifications = [];
	source = globals.baseUrl+'/city/:my_own_keyword';

	professional:Professional;
	tabActive:number;
	tabSubActive:number;
	subtypes;
	username_already:boolean = false;

	items: AngularFireList<any[]>;
        selectedFiles: FileList
        currentFileUpload: FileUpload;
        progress: number = 0;
	//basePath = globals.storageUrl + localStorage.getItem('id');
	basePath = globals.storageUrl;
        images = [];
        uploading: boolean = false;

	constructor(
		private utilService:UtilService,
		private registerService:RegisterService,
		private router:Router,
		private fb: FormBuilder,
		private locationService: LocationService,
		private dataService: DataService,
        private validationMessagesService: ValidationMessagesService
	) {
		this.professional = new Professional;
		this.professional.professional_image = "";
		this.professional.fkprofession = "0";
		this.professional.fkspecialities = [];
		this.professional.fkqualifications = [];
		this.professional.step = 0;
		this.professional.professional_about = "<p>Sed varius nisl id malesuada rutrum. Praesent sodales mattis rhoncus. Cras in sem a est euismod maxim us nec sed dui. Curabitur id nisl condimentum, sodales mauris eget, tempus felis.</p><p>Aenean in porttitor libero, vitae euismod dolor. Aliquam vel sagittis est, vitae volutpat nulla. Inte ger cursus sagittis erat at rhoncus. Sed blandit feugiat imperdiet. Nunc aliquam ut nunc eget accumsan. Nunc ut nisi eget augue aliquet porta a vitae justo.</p><p>Sed varius nisl id malesuada rutrum. Praesent sodales mattis rhoncus. Cras in sem a est euismod maxim us nec sed dui. Curabitur id nisl condimentum, sodales mauris eget, tempus felis.</p><p>Aenean in porttitor libero, vitae euismod dolor. Aliquam vel sagittis est, vitae volutpat nulla. Inte ger cursus sagittis erat at rhoncus. Sed blandit feugiat imperdiet. Nunc aliquam ut nunc eget accumsan. Nunc ut nisi eget augue aliquet porta a vitae justo.";
		this.professional.professional_approach = "<p>Sed varius nisl id malesuada rutrum. Praesent sodales mattis rhoncus. Cras in sem a est euismod maxim us nec sed dui. Curabitur id nisl condimentum, sodales mauris eget, tempus felis.</p><p>Aenean in porttitor libero, vitae euismod dolor. Aliquam vel sagittis est, vitae volutpat nulla. Inte ger cursus sagittis erat at rhoncus. Sed blandit feugiat imperdiet. Nunc aliquam ut nunc eget accumsan. Nunc ut nisi eget augue aliquet porta a vitae justo.</p><p>Sed varius nisl id malesuada rutrum. Praesent sodales mattis rhoncus. Cras in sem a est euismod maxim us nec sed dui. Curabitur id nisl condimentum, sodales mauris eget, tempus felis.</p><p>Aenean in porttitor libero, vitae euismod dolor. Aliquam vel sagittis est, vitae volutpat nulla. Inte ger cursus sagittis erat at rhoncus. Sed blandit feugiat imperdiet. Nunc aliquam ut nunc eget accumsan. Nunc ut nisi eget augue aliquet porta a vitae justo.";
		this.professional.professional_stories = "<p>Sed varius nisl id malesuada rutrum. Praesent sodales mattis rhoncus. Cras in sem a est euismod maxim us nec sed dui. Curabitur id nisl condimentum, sodales mauris eget, tempus felis.</p><p>Aenean in porttitor libero, vitae euismod dolor. Aliquam vel sagittis est, vitae volutpat nulla. Inte ger cursus sagittis erat at rhoncus. Sed blandit feugiat imperdiet. Nunc aliquam ut nunc eget accumsan. Nunc ut nisi eget augue aliquet porta a vitae justo.</p><p>Sed varius nisl id malesuada rutrum. Praesent sodales mattis rhoncus. Cras in sem a est euismod maxim us nec sed dui. Curabitur id nisl condimentum, sodales mauris eget, tempus felis.</p><p>Aenean in porttitor libero, vitae euismod dolor. Aliquam vel sagittis est, vitae volutpat nulla. Inte ger cursus sagittis erat at rhoncus. Sed blandit feugiat imperdiet. Nunc aliquam ut nunc eget accumsan. Nunc ut nisi eget augue aliquet porta a vitae justo.";
		this.tabActive = 0;
		this.tabSubActive = 0;
	}

	ngOnInit(): void {
		this.buildForm();
		this.locationService.getCountrys('all').subscribe(data => this.countries = data);
		this.dataService.getProfessions().subscribe(data => this.professions = data);
		this.dataService.getSpecialities().subscribe(data => this.specialities = data);
		this.dataService.getQualification().subscribe(data => this.qualifications = data);
		var config = {
			apiKey: "AIzaSyB-HNsmjdjWu5MN5fAzP6id1kIVWh4dH74",
			authDomain: "paindown-770af.firebaseapp.com",
			databaseURL: "https://paindown-770af.firebaseio.com",
			projectId: "paindown-770af",
			storageBucket: "paindown-770af.appspot.com",
			messagingSenderId: "357958398702"
		};
		if (!firebase.apps.length) {
			firebase.initializeApp(config);
		}
	}
	
	buildForm(): void {
		
		this.step1Form = this.fb.group({
                name: new FormControl('', {
                        validators: [Validators.required],
                }),
                username: new FormControl('', {
                        validators: [Validators.required,
                        Validators.minLength(4)],
                }),
                email: new FormControl('', {
                        validators: [Validators.required,
                        Validators.pattern("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")],
                }),
                phone: new FormControl('', {
                	validators: [Validators.required]
               	}),
                password: new FormControl('', {
                        validators: [Validators.required,
                        Validators.minLength(8)],
                }),
                repeat: new FormControl('', {
                        validators: [Validators.required,
                        Validators.minLength(8)],
                }),
        	}, { validator: this.passwordMatchValidator, asyncValidator: this.checkUsername.bind(this) });
        	this.step2Form = this.fb.group({
                	country: new FormControl('', {
                	}),
                	region: new FormControl('', {
                	}),
                	city: new FormControl('', {
                	}),
                	address: new FormControl('', {
                        	validators: [Validators.required]
                	}),
        	});
        	this.step3Form = this.fb.group({
                	profession: new FormControl('', {
                	}),
                	website: new FormControl('', {
                	}),
        	});
        	this.step4Form = this.fb.group({
                	about: new FormControl('', {
                	}),
        	});
        	this.step5Form = this.fb.group({
                	approach: new FormControl('', {
                	}),
		});
        	this.step6Form = this.fb.group({
                	stories: new FormControl('', {
                	}),
		});
        	this.step7Form = this.fb.group({
                	specialities: new FormControl('', {
                	}),
		});
        	this.step8Form = this.fb.group({
                	qualifications: new FormControl('', {
                	}),
		});
		this.activeForm = this.step1Form;
        	this.validationMessagesService.seeForErrors(this.step1Form).subscribe((errors: MessageBag) => {
			this.errors = errors;
		});
		this.validationMessagesService.seeForErrors(this.step2Form).subscribe((errors: MessageBag) => {
                	this.errors = errors;
        	});
	}

	profileImgClick(event) {

		$("#profile-image").trigger('click');

	}

	selectFile(event) {

		this.selectedFiles = event.target.files;
                this.upload();

        }

	upload() {
                let file = this.selectedFiles.item(0);
                let ext = '.' + file.name.split('.').pop();
                let uuid = UUID.UUID();
                var blob = file.slice(0, -1, file.type);
                var newFile = new File([blob], uuid + ext, {type: file.type});
                this.currentFileUpload = new FileUpload(newFile);
                this.pushFileToStorage(this.currentFileUpload);
        }

        pushFileToStorage(fileUpload: FileUpload) {
                this.uploading = true;
                const storageRef = firebase.storage().ref();
		const uploadTask = storageRef.child(`${this.basePath}/${fileUpload.file.name}`).put(fileUpload.file);
                uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                  (snapshot) => {
                        // in progress
                        const snap = snapshot as firebase.storage.UploadTaskSnapshot
                        this.progress = Math.round((snap.bytesTransferred / snap.totalBytes) * 100)
                  },
                  (error) => {
                        // fail
                        console.log(error)
			this.utilService.alertError('There was an issue uploading your profile image, please try again.');
                        this.uploading = false;
                  },
                  () => {
                        // success
                        fileUpload.url = uploadTask.snapshot.downloadURL
                        fileUpload.name = fileUpload.file.name
                        //this.uploadService.saveFileData(fileUpload)
                        this.utilService.images.push(fileUpload.url);
			this.professional.professional_image = fileUpload.url;
                        this.uploading = false;
                  }
                );
        }

	onSelectCountry($value){
		this.locationService.getRegions($value.value).subscribe(data => this.regions = data);
		this.professional.fkcountry = $value.data[0].id;
	}

	onSelectRegion($value){
		this.locationService.getCities($value.value).subscribe(data => this.cities = data);
		this.professional.fkregion = $value.data[0].id;
	}

	onSelectCity($value){
		this.professional.fkcity = $value.data[0].id;
	}
	
	passwordMatchValidator(g: FormGroup) {
		return g.get('password').value === g.get('repeat').value ? null : {'mismatch': true};
	 }


	checkUsername(g: FormGroup) {
                return this.dataService.checkUsername(g.get('username').value).pipe(map(data => {
                        return typeof data["error"] != 'undefined' ? null : {'taken': true};
                }));
        }

	myListFormatter(data: any): string {
    		return `${data['value']}`;
    	}

	next(){
		this.getActiveForm();
		if(this.activeForm.valid){
			if(this.tabActive == 8)
				this.sendData();
			else{
				this.tabActive = this.tabActive + 1;
				this.setActiveForm();
			}	
		}
	}

	back(){
		this.tabActive = this.tabActive - 1;
		this.getActiveForm();	
	}

	getActiveForm(){
		switch(this.tabActive){
			case 0:
				this.activeForm = this.step1Form;
			break;
			case 1:
				this.activeForm = this.step2Form;
			break;
			case 2:
				this.activeForm = this.step3Form;
			break;
			case 3:
				this.activeForm = this.step4Form;
			break;
			case 4:
				this.activeForm = this.step5Form;
			break;
			case 5:
				this.activeForm = this.step6Form;
			break;
			case 6:
				this.activeForm = this.step7Form;
			break;
			case 7:
				this.activeForm = this.step8Form;
			break;
		}
	}

	setActiveForm(){
		switch(this.tabActive){
			case 0:
				this.activeForm = this.step1Form;
			break;
			case 1:
				this.activeForm = this.step2Form;
			break;
			case 2:
				this.activeForm = this.step3Form;
			break;
			case 3:
				this.activeForm = this.step4Form;
			break;
			case 4:
				this.activeForm = this.step5Form;
			break;
			case 5:
				this.activeForm = this.step6Form;
			break;
			case 6:
				this.activeForm = this.step7Form;
			break;
			case 7:
				this.activeForm = this.step8Form;
			break;
		}
	}

	changeSubTab(subTab:number){
		this.tabSubActive = subTab;
	}

	sendData(){
		this.utilService.blockUiStart();
		this.professional.fkspecialities = this.selectedSpecialities;
		this.professional.fkqualifications = this.selectedQualifications;
                this.registerService.registerProfessional(this.professional).then( data=>{
                        this.utilService.blockUiStop();
                        data.status == true ? this.utilService.alertSuccess(data.message) : this.utilService.alertError(data.message);
		});
	}
}
