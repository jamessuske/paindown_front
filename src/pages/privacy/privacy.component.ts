import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';

import { UserService } from '../../services/user.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})

export class PrivacyComponent implements OnInit {

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router,  private utilService: UtilService ) {

    title.setTitle('.:Paindown:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
  }

}
