import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UtilService } from '../../services/util.service';

import { UserService } from '../../services/user.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
  
  user = { email: '' }
  error:boolean = false;

  constructor(meta: Meta, title: Title, private userService: UserService, private router:Router,  private utilService: UtilService ) {

    title.setTitle('.:Forgot:.');

    meta.addTags([
      {
        name: 'author', content: ''
      },
      {
        name: 'keywords', content: ''
      },
      {
        name: 'description', content: ''
      },
    ])

  }

  ngOnInit() {
  }

  login(){
    this.utilService.blockUiStart();
    this.userService.forgot(this.user).then(data=>{
      let response = this.utilService.mapPost(data);
      this.utilService.blockUiStop();
      this.utilService.alertSuccess(response.response);
      this.router.navigateByUrl('login');
    }).catch(error=>{
      this.utilService.blockUiStop();
      this.utilService.alertError('The email address enter is not assciated with an account.');
      this.error = true;
    });
  }


}
