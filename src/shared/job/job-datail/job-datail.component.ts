import { Component, OnInit, ViewEncapsulation , Input } from '@angular/core';

@Component({
  selector: 'app-job-datail',
  templateUrl: 'job-detail.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class JobDatailComponent implements OnInit {
	
	@Input() job;
	images = [];
	
	constructor() {}

	ngOnInit() {
		for (let i of this.job.job.images) {
			this.images.push(i.image_url);
		}
	}

}
