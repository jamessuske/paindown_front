import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import * as globals from '../../../app/globals';
import {Observable} from 'rxjs/Rx';
import { UserService } from '../../../services/user.service';
import { UtilService } from '../../../services/util.service';

/*INSTALL*/
import swal from 'sweetalert2';

@Component({
	selector: 'app-public-professional',
	templateUrl: './public.component.html',
	styleUrls: ['./public.component.css']
})
export class PublicProfessionalComponent implements OnInit {

	tabActive:number;
	@Input() user: any;
	review = { problem: '1', rating: 0.0, text: '', user: '' }
	error:boolean = false;
	defaultImage:string='/assets/images/default.png';
	session:boolean= false;
	selectedReviewSort = "0";
	isFirstOpen = true;

	constructor(private userService: UserService, private utilService: UtilService){
		this.tabActive = 0;
	}

	ngOnInit() {
		if (localStorage.getItem('token')) {
			this.session = true;
		}
		this.review.user = this.user.username;
		this.userService.getProfileByUsername(this.user.username, this.selectedReviewSort).subscribe(data => {
        		this.user = data;
			this.user.review_order = "0";
			console.log(this.user);
      		});
	}
	changeTab(tab:number){
		this.tabActive = tab;
	}
	onRatingChange($event){
		this.review.rating = $event.rating;
		console.log($event.rating);
	}

	onReviewSortChange($event) {

		this.selectedReviewSort = this.user.review_order;
		this.utilService.blockUiStart();
		this.userService.getProfileByUsername(this.user.username, this.selectedReviewSort).subscribe(data => {
        		this.user = data;
			this.user.review_order = this.selectedReviewSort;
			console.log(this.user);
                	this.utilService.blockUiStop();
      		});

	}

	submitReview() {
		this.selectedReviewSort = this.user.review_order;
		this.utilService.blockUiStart();
        	this.userService.review(this.review).then(data=>{
                	let response = this.utilService.mapPost(data);
			this.userService.getProfileByUsername(this.user.username, this.selectedReviewSort).subscribe(data => {
        			this.user = data;
				this.user.review_order = this.selectedReviewSort;
                		this.utilService.blockUiStop();
                		this.utilService.alertSuccess(response.response);
      			});
         	}).catch(error=>{
                	this.utilService.blockUiStop();
                	this.utilService.alertError("Something went wrong, please try again later.");
                	this.error = true;
         	});
	}
	
}
