import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user';

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.css']
})
export class ProfileHeaderComponent implements OnInit {

	user:User;

	constructor(){

	}

	ngOnInit(){
		this.user = new User;
		this.user.user_name = localStorage.getItem('user');
		this.user.register = localStorage.getItem('register');
		this.user.photo = localStorage.getItem('photo');
		this.user.cover = localStorage.getItem('cover');
	}

}
