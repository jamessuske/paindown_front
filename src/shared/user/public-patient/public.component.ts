import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import * as globals from '../../../app/globals';
import {Observable} from 'rxjs/Rx';

/*INSTALL*/
import swal from 'sweetalert2';

@Component({
	selector: 'app-public-patient',
	templateUrl: './public.component.html',
	styleUrls: ['./public.component.css']
})
export class PublicPatientComponent implements OnInit {

	tabActive:number;
	@Input() user: any;
	review = {rating:4}
	defaultImage:string='/assets/images/default.png';
	isFirstOpen = true;

	constructor(){
		this.tabActive = 0;
	}

	ngOnInit() {
		console.log(this.user);
	}
	changeTab(tab:number){
		this.tabActive = tab;
	}
	
}
