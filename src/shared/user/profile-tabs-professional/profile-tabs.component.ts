import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as globals from '../../../app/globals';
import {Observable} from 'rxjs/Rx';

/*INSTALL*/
import swal from 'sweetalert2';
import {IMyDpOptions} from 'mydatepicker';
import {AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { UUID } from 'angular2-uuid';
import * as firebase from 'firebase/app';

/*SERVICES*/
import { UserService } from '../../../services/user.service'
import { UtilService } from '../../../services/util.service';
import { LocationService } from '../../../services/location.service';
import { DataService } from '../../../services/data.service';

/*MODELS*/
import { User } from '../../../models/user';
/*import { TradesCategory } from '../../../models/TradesCategory';
import { TradesSubCategory } from '../../../models/TradesSubCategory';
import { Priority } from '../../../models/Priority';
import { Job } from '../../../models/Job';*/
import { Country } from '../../../models/Country';
import { Region } from '../../../models/Region';
import { City } from '../../../models/City';
import {FileUpload} from '../../../models/FileUpload';

@Component({
	selector: 'app-profile-tabs-prof',
	templateUrl: './profile-tabs.component.html',
	styleUrls: ['./profile-tabs.component.css']
})
export class ProfileTabsProfessionalComponent implements OnInit {

	tabActive:number;
	subTabActive:number;
	user:User;
	country:Country;
	region:Region;

	countries = [];
	regions = {};
	cities = {};
	painTypes = [];
	painLevels = [];
	painkillers = [];
	professions = [];
	specialities = [];
	qualification = [];
	countrySelected = '';
	regionSelected = '';
	citySelected = '';
	countryInit:boolean = false;
	regionInit:boolean = false;
	cityInit:boolean = false;
	qualificationSelected = '';
	specialitiesSelected = '';
	defaultImage:string='/assets/images/default.png';

	items: AngularFireList<any[]>;
        selectedFiles: FileList
        currentFileUpload: FileUpload;
        progress: number = 0;
        //basePath = globals.storageUrl + localStorage.getItem('id');
        basePath = globals.storageUrl;
        images = [];
        uploading: boolean = false;

	password = { email: '', password: '', newpassword: '', password_confirmation: '' }
	error:boolean = false;

	city = '';
	myDatePickerOptions = this.utilService.myDatePickerOptions;

	constructor(private userService:UserService, private utilService: 
		UtilService, private ref:ChangeDetectorRef, private locationService: LocationService,
		private dataService: DataService){
		this.tabActive = 0;
		this.subTabActive = 0;
		this.user = new User;
		this.user.image = globals.imageUserDefault;
		this.user.qualifications = [];
		this.user.specialities = [];
		this.user.preference = [];
		this.user.about = "";
		this.user.approach = "";
		this.user.stories = "";
	}

	ngOnInit(){
		this.utilService.blockUiStart();
		var config = {
                        apiKey: "AIzaSyB-HNsmjdjWu5MN5fAzP6id1kIVWh4dH74",
                        authDomain: "paindown-770af.firebaseapp.com",
                        databaseURL: "https://paindown-770af.firebaseio.com",
                        projectId: "paindown-770af",
                        storageBucket: "paindown-770af.appspot.com",
                        messagingSenderId: "357958398702"
                };
		if (!firebase.apps.length) {
			firebase.initializeApp(config);
		}
		this.dataService.getProfessions().subscribe(data => this.professions = data);
		this.dataService.getQualification().subscribe(data => this.qualification = data);
		this.dataService.getSpecialities().subscribe(data => this.specialities = data);
		this.userService.profile().subscribe(data=>{
			this.user = data;
			console.log(this.user);
			this.locationService.getCountrys('all').subscribe(data => {
                                this.countries = data;
                                this.countrySelected = this.user.location.country.id;
                                this.locationService.getRegions(this.countrySelected).subscribe(data => {
                                        this.regions = data;
                                        this.regionSelected = this.user.location.region.id;
                                        this.locationService.getCities(this.regionSelected).subscribe(data => {
                                                this.cities = data;
                                                this.citySelected = this.user.location.city.id;
                                        });
                                });
                        });
			this.user.isPainkillers = this.user.usage_painkiller == 0 ? true : false;
			this.user.public = this.user.public == 1 ? true : false;
			this.password.email = data.email;
			this.utilService.blockUiStop();
		});
	}

	changeTab(tab:number){
		this.tabActive = tab;
		if(tab == 0){
		//this.getUserData();
		}
	}

	changeSubTab(tab:number){
		this.subTabActive = tab;
	}


	profileImgClick(event) {
                $("#profile-image").trigger('click');
        }
        selectFile(event) {
                this.selectedFiles = event.target.files;
                this.upload();
        }
        upload() {
                let file = this.selectedFiles.item(0);
                let ext = '.' + file.name.split('.').pop();
                let uuid = UUID.UUID();
                var blob = file.slice(0, -1, file.type);
                var newFile = new File([blob], uuid + ext, {type: file.type});
                this.currentFileUpload = new FileUpload(newFile);
                this.pushFileToStorage(this.currentFileUpload);
        }
        pushFileToStorage(fileUpload: FileUpload) {
                this.uploading = true;
                const storageRef = firebase.storage().ref();
                const uploadTask = storageRef.child(`${this.basePath}/${fileUpload.file.name}`).put(fileUpload.file);
                uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                  (snapshot) => {
                        // in progress
                        const snap = snapshot as firebase.storage.UploadTaskSnapshot
                        this.progress = Math.round((snap.bytesTransferred / snap.totalBytes) * 100)
                  },
                  (error) => {
                        // fail
                        console.log(error)
                        this.utilService.alertError('There was an issue uploading your profile image, please try again.');
                        this.uploading = false;
                  },
                  () => {
                        // success
                        fileUpload.url = uploadTask.snapshot.downloadURL
                        fileUpload.name = fileUpload.file.name
                        //this.uploadService.saveFileData(fileUpload)
                        this.utilService.images.push(fileUpload.url);
                        this.user.professional_image = fileUpload.url;
                        this.uploading = false;
                  }
                );
        }

	onSelectCountry($value){
                if(this.countryInit == true)
                {
                        this.locationService.getRegions($value.value).subscribe(data => {
                                this.regions = [];
                                this.regions = data;
                                this.user.location.country.id = $value.value;
                        });
                }
                else
                {
                        this.countryInit = true;
                }
        }
        onSelectRegion($value){
                if(this.regionInit == true)
                {
                        this.locationService.getCities($value.value).subscribe(data => {
                                this.cities = [];
                                this.cities = data;
                                this.user.location.region.id = $value.value;
                        });
                }
                else
                {
                        this.regionInit = true;
                }
        }
        onSelectCity($value){
                if(this.cityInit == true)
                {
                        this.user.location.city.id = $value.value;
                }
                else
                {
                        this.cityInit = true;
                }
        }

	onSelectSpecialities($value) {

		if(!this.user.specialities.some(e => e.id == $value.value)) {
			this.user.specialities.push({id: $value.value, name: $value.data[0].text});
		}

	}

	onSelectQualifications($value) {

		if(!this.user.qualifications.some(e => e.id == $value.value)) {
			this.user.qualifications.push({id: $value.value, name: $value.data[0].text});
		}



	}

	removeSpecialities($value) {

		this.user.specialities = $.grep(this.user.specialities, function(e){ 
			return e.id != $value; 
		});

	}

	removeQualifications($value) {
	
		this.user.qualifications = $.grep(this.user.qualifications, function(e){ 
			return e.id != $value; 
		});

	}


	preferenceChange($event)
	{
		console.log($event.target.getAttribute('name'));
		if($event.target.checked)
                {
			this.user.preference = $.grep(this.user.preference, function(e) {
				if(e.pkpreference == $event.target.getAttribute('name'))
				{
					e.patientpreference_selected = 1;
				}
				return true;
			});
                }
                else
                {
			this.user.preference = $.grep(this.user.preference, function(e) {
				if(e.pkpreference == $event.target.getAttribute('name'))
				{
					e.patientpreference_selected = 0;
				}
				return true;
			});
                }
		console.log(this.user.preference);
	}


	savePerferences(){
                this.utilService.blockUiStart();
                this.userService.updatePerference(this.user.preference).then(data=>{
                        let response = this.utilService.mapPost(data);
                        this.utilService.blockUiStop();
                        this.utilService.alertSuccess(response.response);
                }).catch(error=>{
                        this.utilService.blockUiStop();
                        this.utilService.alertError('There was an issue updating your settings, please try again.');
                        this.error = true;
                });
                
        }

	save(){
		this.utilService.blockUiStart();
		console.log(this.user);
		this.userService.update(this.user).then( data=>{
			if (data.status === true) {
				this.dataService.getProfessions().subscribe(data => this.professions = data);
				this.dataService.getQualification().subscribe(data => this.qualification = data);
				this.dataService.getSpecialities().subscribe(data => this.specialities = data);
				this.userService.profile().subscribe(data=>{
					this.user = data;
					console.log(this.user);
					this.locationService.getCountrys('all').subscribe(data => {
                                		this.countries = data;
                                		this.countrySelected = this.user.location.country.id;
                                		this.locationService.getRegions(this.countrySelected).subscribe(data => {
                                        		this.regions = data;
                                        		this.regionSelected = this.user.location.region.id;
                                        		this.locationService.getCities(this.regionSelected).subscribe(data => {
                                                		this.cities = data;
                                                		this.citySelected = this.user.location.city.id;
                                        		});
                                		});
                        		});
					this.user.isPainkillers = this.user.usage_painkiller == 0 ? true : false;
					this.user.public = this.user.public == 1 ? true : false;
					this.password.email = data.email;
					this.utilService.blockUiStop();
					this.utilService.alertSuccess('your information has been updated');
				});
			}else{
				this.utilService.alertError(data.message);
			}
		}).catch(error=>{
			this.utilService.blockUiStop();
		});
	}


	savePassword(){

		this.utilService.blockUiStart();
        	this.userService.updatePassword(this.password).then(data=>{
                	let response = this.utilService.mapPost(data);
                	this.utilService.blockUiStop();
                	this.utilService.alertSuccess(response.response);
         	}).catch(error=>{
                	this.utilService.blockUiStop();
                	this.utilService.alertError('There was an issue resetting your password, please try again.');
                	this.error = true;
         	});


	}
	
}
