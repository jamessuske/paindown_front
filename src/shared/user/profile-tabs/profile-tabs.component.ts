import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as globals from '../../../app/globals';
import {Observable} from 'rxjs/Rx';

/*INSTALL*/
import swal from 'sweetalert2';
import {IMyDpOptions} from 'mydatepicker';
import {AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { UUID } from 'angular2-uuid';
import * as firebase from 'firebase/app';

/*SERVICES*/
import { UserService } from '../../../services/user.service';
import { UtilService } from '../../../services/util.service';
import { LocationService } from '../../../services/location.service';
import { DataService } from '../../../services/data.service';

/*MODELS*/
import { User } from '../../../models/user';
/*import { TradesCategory } from '../../../models/TradesCategory';
import { TradesSubCategory } from '../../../models/TradesSubCategory';
import { Priority } from '../../../models/Priority';
import { Job } from '../../../models/Job';*/
import { Country } from '../../../models/Country';
import { Region } from '../../../models/Region';
import { City } from '../../../models/City';
import {FileUpload} from '../../../models/FileUpload';

@Component({
	selector: 'app-profile-tabs',
	templateUrl: './profile-tabs.component.html',
	styleUrls: ['./profile-tabs.component.css']
})
export class ProfileTabsComponent implements OnInit {

	tabActive:number;
	user:User;
	country:Country;
	region:Region;

	countries = [];
	regions = [];
	cities = [];
	painTypes = [];
	painLevels = [];
	painkillers = [];
	countrySelected = '';
	regionSelected = '';
	citySelected = '';
	countryInit:boolean = false;
	regionInit:boolean = false;
	cityInit:boolean = false;
	defaultImage:string='/assets/images/default.png';

	password = { email: '', password: '', newpassword: '', password_confirmation: '' }
	error:boolean = false;

	city = '';
	myDatePickerOptions = this.utilService.myDatePickerOptions;
	source = globals.baseUrl+'/city/:my_own_keyword';

	jobs = [];

	items: AngularFireList<any[]>;
	selectedFiles: FileList
	currentFileUpload: FileUpload;
	progress: number = 0;
	basePath = globals.storageUrl + localStorage.getItem('id');
	images = [];
	uploading: boolean = false;

	constructor(private userService:UserService, private utilService: 
		UtilService, private ref:ChangeDetectorRef, private locationService: LocationService,
		private dataService: DataService){
		this.tabActive = 0;
		this.user = new User;
		this.user.photo = globals.imageUserDefault;
		this.user.isPainkillers = false;
		this.user.preference = [];
	}

	ngOnInit() {
		this.utilService.blockUiStart();
		this.dataService.getPainTypes().subscribe(data => this.painTypes = data);
		this.dataService.getPainLevel().subscribe(data => this.painLevels = data);
		this.dataService.getPainKiller().subscribe(data => this.painkillers = data);
		this.userService.profile().subscribe(data=>{
			this.user = data;
			this.locationService.getCountrys('all').subscribe(data => {
				this.countries = data;
				this.countrySelected = this.user.location.country.id;
				this.locationService.getRegions(this.countrySelected).subscribe(data => {
					this.regions = data;
					this.regionSelected = this.user.location.region.id;
					this.locationService.getCities(this.regionSelected).subscribe(data => { 
						this.cities = data;
						this.citySelected = this.user.location.city.id;
					});
				});
			});
			this.user.isPainkillers = this.user.usage_painkiller == 0 ? true : false;
			this.user.public = this.user.public == 1 ? true : false;
			this.password.email = data.email;
			this.utilService.blockUiStop();
		});
		var config = {
                        apiKey: "AIzaSyB-HNsmjdjWu5MN5fAzP6id1kIVWh4dH74",
                        authDomain: "paindown-770af.firebaseapp.com",
                        databaseURL: "https://paindown-770af.firebaseio.com",
                        projectId: "paindown-770af",
                        storageBucket: "paindown-770af.appspot.com",
                        messagingSenderId: "357958398702"
                };
		if (!firebase.apps.length) {
			firebase.initializeApp(config);
		}
	}

	myListFormatter(data: any): string {
    		return `${data['value']}`;
    	}

	changeTab(tab:number){
		this.tabActive = tab;
		if(tab == 0){
		//this.getUserData();
		}
	}

	onSelectCountry($value){
		if(this.countryInit == true)
		{
			this.locationService.getRegions($value.value).subscribe(data => {
				this.regions = [];
				this.regions = data;
				this.user.location.country.id = $value.value;
				this.user.location.country.name = $value.data[0].text;
			});
		}
		else
		{
			this.countryInit = true;
		}
	}

	onSelectRegion($value){
		if(this.regionInit == true)
		{
			this.locationService.getCities($value.value).subscribe(data => { 
				this.cities = [];
				this.cities = data;
				this.user.location.region.id = $value.value;
				this.user.location.region.name = $value.data[0].text;
			});
		}
		else
		{
			this.regionInit = true;
		}
	}

	onSelectCity($value){
		if(this.cityInit == true)
		{
			this.user.location.city.id = $value.value;
			this.user.location.city.name = $value.data[0].text;
		}
		else
		{
			this.cityInit = true;
		}
	}


	preferenceChange($event)
	{
		if($event.target.checked)
                {       
                        this.user.preference = $.grep(this.user.preference, function(e) {
                                if(e.pkpreference == $event.target.getAttribute('name'))
                                {       
                                        e.patientpreference_selected = 1;
                                }
                                return true;
                        });
                }
                else
                {       
                        this.user.preference = $.grep(this.user.preference, function(e) {
                                if(e.pkpreference == $event.target.getAttribute('name'))
                                {       
                                        e.patientpreference_selected = 0;
                                }
                                return true;
                        });
                }
	}

	save(){
		this.utilService.blockUiStart();
		if(this.user.isPainkillers == true)
		{
			this.user.usage_painkiller = 0;
		}
		this.userService.update(this.user).then( data=>{
			if (data.status === true) {
				this.dataService.getPainTypes().subscribe(data => this.painTypes = data);
				this.dataService.getPainLevel().subscribe(data => this.painLevels = data);
				this.dataService.getPainKiller().subscribe(data => this.painkillers = data);
				this.userService.profile().subscribe(data=>{
					this.user = data;
					this.locationService.getCountrys('all').subscribe(data => {
						this.countries = data;
						this.countrySelected = this.user.location.country.id;
						this.locationService.getRegions(this.countrySelected).subscribe(data => {
							this.regions = data;
							this.regionSelected = this.user.location.region.id;
							this.locationService.getCities(this.regionSelected).subscribe(data => { 
								this.cities = data;
								this.citySelected = this.user.location.city.id;
							});
						});
					});
					this.user.isPainkillers = this.user.usage_painkiller == 0 ? true : false;
					this.user.public = this.user.public == 1 ? true : false;
					this.password.email = data.email;
					this.utilService.blockUiStop();
					this.utilService.alertSuccess('your information has been updated');
				});
			}else{
				this.utilService.alertError(data.message);
			}
		}).catch(error=>{
			this.utilService.blockUiStop();
		});
	}


	savePassword(){

		this.utilService.blockUiStart();
                this.userService.updatePassword(this.password).then(data=>{
                        let response = this.utilService.mapPost(data);
                        this.utilService.blockUiStop();
                        this.utilService.alertSuccess(response.response);
                }).catch(error=>{
                        this.utilService.blockUiStop();
                        this.utilService.alertError('There was an issue resetting your password, please try again.');
                        this.error = true;
                });
	

	}

	savePerferences(){
		this.utilService.blockUiStart();
                this.userService.updatePerference(this.user.preference).then(data=>{
                        let response = this.utilService.mapPost(data);
                        this.utilService.blockUiStop();
                        this.utilService.alertSuccess(response.response);
                }).catch(error=>{
                        this.utilService.blockUiStop();
                        this.utilService.alertError('There was an issue updating your settings, please try again.');
                        this.error = true;
                });
		
	}
	
	changeSms(){
		this.user.sms = !this.user.sms;
	}
	changeNews(){
		this.user.news = !this.user.news;
	}

	savejob(){
		this.utilService.blockUiStart();
		/*this.job.job.start_date = this.job.job.start_date_ar['formatted'];
		this.job.location.id = this.city.id;
		this.job.job.images = this.utilService.images;
		this.postJobService.addJob(this.job).then(data=>{
			this.utilService.blockUiStop();
			if (data.status === true) {
				this.utilService.alertSuccess('your information has been updated');
			}else{
				this.utilService.alertError(data.message);
			}
		}).catch(error=>{
			this.utilService.blockUiStop();
		});*/
	}

	getMyJobs(){
		this.utilService.blockUiStart();
		/*this.postJobService.listjob( localStorage.getItem('token') ).subscribe(
			result =>{
				this.jobs = result;
				this.utilService.blockUiStop();
			}
		);*/
	}

	setJobSelected(job){
		//this.jobSelected = job;
	}

	showListJobs(){
		//this.jobSelected = null;
		this.getMyJobs();
	}

	profileImgClick(event) {
		$("#profile-image").trigger('click');
	}

	selectFile(event) {
		this.selectedFiles = event.target.files;
		this.upload();
	}	

	upload() {
		let file = this.selectedFiles.item(0);
		let ext = '.' + file.name.split('.').pop();
		let uuid = UUID.UUID();
		var blob = file.slice(0, -1, file.type); 
		var newFile = new File([blob], uuid + ext, {type: file.type});
		this.currentFileUpload = new FileUpload(newFile);
		this.pushFileToStorage(this.currentFileUpload);
	}

	pushFileToStorage(fileUpload: FileUpload) {
		this.uploading = true;
		const storageRef = firebase.storage().ref();
		const uploadTask = storageRef.child(`${this.basePath}/${fileUpload.file.name}`).put(fileUpload.file);
	 
		uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
		  (snapshot) => {
			// in progress
			const snap = snapshot as firebase.storage.UploadTaskSnapshot
			this.progress = Math.round((snap.bytesTransferred / snap.totalBytes) * 100)
		  },
		  (error) => {
			// fail
			console.log(error)
			this.utilService.alertError('There was an issue uploading your profile image, please try again.');
			this.uploading = false;
		  },
		  () => {
			// success
			fileUpload.url = uploadTask.snapshot.downloadURL
			fileUpload.name = fileUpload.file.name
			//this.uploadService.saveFileData(fileUpload)
			this.user.patient_image = fileUpload.url;
			this.utilService.images.push(fileUpload.url);
			this.uploading = false;
		  }
		);
	}
	/*updateJob(){
		this.changeTab(1);
		this.job = this.jobSelected;
		this.city.id = this.job.location.id;
		this.city.value = this.job.location.city;
		this.ref.detectChanges();
		console.log(this.city);
	}*/
}
