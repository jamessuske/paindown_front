import { Component, OnInit } from '@angular/core';
import { UtilService  } from '../../../services/util.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
	collapse:boolean;

	showJobs:boolean = true;
	showTradesman:boolean = true;

	session:boolean= false;
	menu:boolean= false;
	active;
	
	constructor(private utilService:UtilService, private router:Router) { 
		this.collapse = false;
	}
	changeCollapse(){
		this.collapse = !this.collapse;
	}

	ngOnInit(){
		if(localStorage.getItem('token') && localStorage.getItem('state') == '3' )
          this.showJobs = false;

        if(localStorage.getItem('token') && localStorage.getItem('state') == '2' )
          this.showTradesman = false;

      	if (localStorage.getItem('token')) 
      		this.session = true;
	}
	logout(){
		localStorage.clear();
		this.router.navigateByUrl('');		
	}

	ngAfterContentChecked(){
		if(localStorage.getItem('token') && localStorage.getItem('state') == '3' )
          this.showJobs = false;

        if(localStorage.getItem('token') && localStorage.getItem('state') == '2' )
          this.showTradesman = false;

      	if (localStorage.getItem('token')) 
      		this.session = true;
	}

	subMenu(){
		this.menu = !this.menu;
	}
}
