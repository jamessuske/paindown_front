import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import {Observable} from 'rxjs/Rx';
import { UtilService } from '../../../services/util.service';

@Component({
  selector: 'figure-image',
  templateUrl: './figure-image.component.html',
  styleUrls: ['./figure-image.component.css']
})
export class FigureComponent {

  @Input() images;

  constructor(private _sanitizer: DomSanitizer, private utilService:UtilService) { }

  ngOnInit(){

  }

	getBackground(image) {
		return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
	}

	deleteElement(position){
		this.images.splice(position, 1);
		console.log( this.utilService.images );
	}

}
