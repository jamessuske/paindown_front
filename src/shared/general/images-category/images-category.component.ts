import { Component, OnInit, Input } from '@angular/core';


@Component({
	selector: 'app-images-category',
	templateUrl: './images-category.component.html',
	styleUrls: ['./images-category.component.css']
})
export class ImagesCategoryComponent implements OnInit {
	@Input() items;
	constructor(){}

	ngOnInit() {
	}

}
