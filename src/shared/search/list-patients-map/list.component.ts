import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import * as globals from '../../../app/globals';
import {Observable} from 'rxjs/Rx';
import { UserService } from '../../../services/user.service';
import { UtilService } from '../../../services/util.service';
import { DataService } from '../../../services/data.service';

/*INSTALL*/
import swal from 'sweetalert2';

@Component({
	selector: 'app-list-patientsMap',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.css']
})
export class ListPatientMapComponent implements OnInit {

	patients = [];
        painTypes = [];
        rangeValues: number[] = [15,45];
        painRanges: number[] = [10, 20];
        val1: string;
        textSearch: string;
        order: String;
        location: String;
        painType: String;
	painKillers: String;
	painYears: String;
	realOrder: String;
        defaultImage:string='/assets/images/default.png';
	//lat: number = 51.678418;
	//lng: number = 7.809007;
	lat: number = 0;
	lng: number = 0;
	longUser: String;
        latUser: String;


	constructor(private userService: UserService, private utilService: UtilService, private dataService: DataService){

		this.textSearch = "";
                this.order = "0";
                this.location = "0";
                this.painType = "0";
		this.val1 = "0";
		this.painKillers = "0";
		this.painYears = "0";
		this.longUser = "0";
                this.latUser = "0";


	}

	ngOnInit() {

		this.utilService.blockUiStart();
		this.userService.getUserLocation().subscribe(data => {
                	this.latUser = data.latitude;
                        this.longUser = data.longitude;
                	this.userService.getAllPatientsMap(this.longUser, this.latUser).subscribe(data => {
                       		this.patients = data;
				this.lat = Number(this.latUser);
				this.lng = Number(this.longUser);
                        	this.dataService.getPainTypes().subscribe(data => {
                                	this.painTypes = data;
                        	});
                        	this.utilService.blockUiStop();
                	});
		});

	}

	openNav() {
                $('.sidebar').addClass('expanded');
        }
        closeNav() {
                $('.sidebar').removeClass('expanded');
        }

	mobileSearch() {
                $('body').addClass('modal-open');
                $('#searchModal').addClass('in');
                $('#searchModal').css('display', 'block');
                $('body').append("<div class='modal-backdrop fade in'></div>");

        }
        mobileCloseSearch() {

                $('body').removeClass('modal-open');
                $('#searchModal').removeClass('in');
                $('#searchModal').css('display', 'none');
                $('.modal-backdrop').remove();


        }

	onSubmit($value){

		this.utilService.blockUiStart();
                this.userService.getMapLocationPatients(this.textSearch).subscribe(data => {
			this.lat = data.latitude;
			this.lng = data.longitude;
                        this.utilService.blockUiStop();
                });

        }

	public onFilters($value){

		if(this.order == "0")
		{
			this.realOrder = this.painYears;
			this.order = this.painYears;
		}
		else if(this.painYears == "0")
		{
			this.realOrder = this.order;
			this.painYears = this.order;
		}
		else
		{
			this.realOrder = "0";
		}

		this.utilService.blockUiStart();
                this.userService.getPatients(this.painType, this.location, this.realOrder, "0", "0", "0", "0", this.painKillers, this.longUser, this.latUser).subscribe(data => {
                        this.patients = data;
                        this.utilService.blockUiStop();
                });

	}

	
}
