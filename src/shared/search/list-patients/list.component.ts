import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import * as globals from '../../../app/globals';
import {Observable} from 'rxjs/Rx';
import { UserService } from '../../../services/user.service';
import { UtilService } from '../../../services/util.service';
import { DataService } from '../../../services/data.service';

/*INSTALL*/
import swal from 'sweetalert2';

@Component({
	selector: 'app-list-patients',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.css']
})
export class ListPatientComponent implements OnInit {

	review = {rating:4}
	patients = [];
	painTypes = [];
	distanceRange: number[] = [15,45];
	painRanges: number[] = [10, 20];
	painkillers: string;
	textSearch: string;
	order: String;
	long: String;
	lat: String;
	location: String;
	painType: String;
	minRangeDistance: number;
	maxRangeDistance: number;
	distanceRangeApply:boolean = false;
	minRangePain: number;
	maxRangePain: number;
	painRangeApply:boolean = false;
	defaultImage:string='/assets/images/default.png';

	constructor(private userService: UserService, private utilService: UtilService, private dataService: DataService){

		this.textSearch = "";
		this.order = "0";
		this.location = "0";
		this.painType = "0";
		this.painkillers = "Neither";
		this.long = "0";
		this.lat = "0";

	}

	ngOnInit() {
		this.utilService.blockUiStart();
		this.userService.getAllPatients().subscribe(data => {
			this.patients = data;
			this.dataService.getPainTypes().subscribe(data => {
				this.painTypes = data;
			});
			this.userService.getUserLocation().subscribe(data => {
                                this.lat = data.latitude;
                                this.long = data.longitude;
                        });
			this.utilService.blockUiStop();
		});
	}

	onSubmit($value){
		this.utilService.blockUiStart();
		this.userService.getPatientsByUsername(this.textSearch).subscribe(data => {
			this.patients = data;
			this.utilService.blockUiStop();
		});
	}

	openNav() {
                $('.sidebar').addClass('expanded');
        }
        closeNav() {
                $('.sidebar').removeClass('expanded');
        }
	mobileSearch() {
                $('body').addClass('modal-open');
                $('#searchModal').addClass('in');
                $('#searchModal').css('display', 'block');
                $('body').append("<div class='modal-backdrop fade in'></div>");

        }
        mobileCloseSearch() {

                $('body').removeClass('modal-open');
                $('#searchModal').removeClass('in');
                $('#searchModal').css('display', 'none');
                $('.modal-backdrop').remove();


        }

	public onFilters($value){ 

		if(this.distanceRangeApply)
                {
                        this.minRangeDistance = this.distanceRange[0];
                        this.maxRangeDistance = this.distanceRange[1];
                }
                else
                {
                        this.minRangeDistance = 0;
                        this.maxRangeDistance = 0;
                }	
		
		if(this.painRangeApply)
                {
                        this.minRangePain = this.painRanges[0];
                        this.maxRangePain = this.painRanges[1];
                }
                else
                {
                        this.minRangePain = 0;
                        this.maxRangePain = 0;
                }

		this.utilService.blockUiStart();
		this.userService.getPatients(this.painType, this.location, this.order, this.minRangeDistance, this.maxRangeDistance, this.minRangePain, this.minRangePain, this.painkillers, this.long, this.lat).subscribe(data => {
			this.patients = data;
			this.utilService.blockUiStop();
		});
	}
	
	public onFiltersPainkillers($event){ 

		this.painkillers = $event;
		console.log(this.painkillers);
		console.log($event);

		if(this.distanceRangeApply)
                {
                        this.minRangeDistance = this.distanceRange[0];
                        this.maxRangeDistance = this.distanceRange[1];
                }
                else
                {
                        this.minRangeDistance = 0;
                        this.maxRangeDistance = 0;
                }	
		
		if(this.painRangeApply)
                {
                        this.minRangePain = this.painRanges[0];
                        this.maxRangePain = this.painRanges[1];
                }
                else
                {
                        this.minRangePain = 0;
                        this.maxRangePain = 0;
                }

		this.utilService.blockUiStart();
		this.userService.getPatients(this.painType, this.location, this.order, this.minRangeDistance, this.maxRangeDistance, this.minRangePain, this.minRangePain, this.painkillers, this.long, this.lat).subscribe(data => {
			this.patients = data;
			this.utilService.blockUiStop();
		});
	}
	
	
	
	public onFilterRangesDistance($value){ 
		
		if(this.painRangeApply)
                {
                        this.minRangePain = this.painRanges[0];
                        this.maxRangePain = this.painRanges[1];
                }
                else
                {
                        this.minRangePain = 0;
                        this.maxRangePain = 0;
                }	

		this.distanceRangeApply = true;
		this.utilService.blockUiStart();
		this.userService.getPatients(this.painType, this.location, this.order, this.distanceRange[0], this.distanceRange[1], this.minRangePain, this.maxRangePain, this.painkillers, this.long, this.lat).subscribe(data => {
			this.patients = data;
			this.utilService.blockUiStop();
		});
	}
	
	public onFilterRangesPain($value){ 

		if(this.distanceRangeApply)
                {
                        this.minRangeDistance = this.distanceRange[0];
                        this.maxRangeDistance = this.distanceRange[1];
                }
                else
                {
                        this.minRangeDistance = 0;
                        this.maxRangeDistance = 0;
                }
	
		this.painRangeApply = true;
		this.utilService.blockUiStart();
		console.log(this.painRanges);
		this.userService.getPatients(this.painType, this.location, this.order, this.minRangeDistance, this.maxRangeDistance, this.painRanges[0], this.painRanges[1], this.painkillers, this.long, this.lat).subscribe(data => {
			this.patients = data;
			this.utilService.blockUiStop();
		});
	}
}
