import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import * as globals from '../../../app/globals';
import {Observable} from 'rxjs/Rx';
import { UserService } from '../../../services/user.service';
import { UtilService } from '../../../services/util.service';
import { DataService } from '../../../services/data.service';

/*INSTALL*/
import swal from 'sweetalert2';

@Component({
	selector: 'app-list-professional',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.css']
})
export class ListProfessionalComponent implements OnInit {

	review = {rating:4}
	professionals = [];
	professions = [];
        specialities = [];
	textSearch: String;
	order: String;
	location: String;
        pros: String;
        spec: String;
	long: String;
	lat: String;
	distanceRange: number[] = [15,45];
	minRange: number;
	maxRange: number;
	distanceRangeApply:boolean = false;
	defaultImage:string='/assets/images/default.png';

	constructor(private userService: UserService, private utilService: UtilService, private dataService: DataService){

		this.textSearch  = "";
		this.order = "0";
		this.location = "0";
                this.pros = "0";
                this.spec = "0";
		this.long = "0";
		this.lat = "0";

	}

	ngOnInit() {
		this.utilService.blockUiStart();
                this.userService.getAllProfessionals().subscribe(data => {
                        this.professionals = data;
                        this.dataService.getProfessions().subscribe(data => {
                                this.professions = data;
                        });
                        this.dataService.getSpecialities().subscribe(data => {
                                this.specialities = data;
                        });
			this.userService.getUserLocation().subscribe(data => {
				this.lat = data.latitude;
				this.long = data.longitude;
			});
                        this.utilService.blockUiStop();
                });
		

	}


	openNav() {
		$('.sidebar').addClass('expanded');
	}

	closeNav() {
		$('.sidebar').removeClass('expanded');
	}
	mobileSearch() {
		$('body').addClass('modal-open');
		$('#searchModal').addClass('in');
		$('#searchModal').css('display', 'block');
		$('body').append("<div class='modal-backdrop fade in'></div>");

	}
	mobileCloseSearch() {

		$('body').removeClass('modal-open');
		$('#searchModal').removeClass('in');
		$('#searchModal').css('display', 'none');
		$('.modal-backdrop').remove();
		

	}

	onSubmit($value){
                this.utilService.blockUiStart();
                this.userService.getProfessionalsByUsername(this.textSearch).subscribe(data => {
                        this.professionals = data;
            		this.utilService.blockUiStop();
                });
        }

	public onFilters($value){
                this.utilService.blockUiStart();
		console.log(this.distanceRangeApply);
		if(this.distanceRangeApply == true)
		{
			this.minRange = this.distanceRange[0];
			this.maxRange = this.distanceRange[1];
		}
		else
		{
			this.minRange = 0;
			this.maxRange = 0;
		}
		console.log(this.minRange);
		console.log(this.maxRange);
                this.userService.getProfessionals(this.pros, this.spec, this.location, this.order, this.minRange, this.maxRange, this.long, this.lat).subscribe(data => {
                        this.professionals = data;
            		this.utilService.blockUiStop();
                });
	}
	

	public onFilterRanges($value)
	{
		console.log("Here");
		this.distanceRangeApply = true;	
                this.utilService.blockUiStart();
                this.userService.getProfessionals(this.pros, this.spec, this.location, this.order, this.distanceRange[0], this.distanceRange[1], this.long, this.lat).subscribe(data => {
                        this.professionals = data;
            		this.utilService.blockUiStop();
                });
	}

	
}
