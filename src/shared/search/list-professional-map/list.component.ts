import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import * as globals from '../../../app/globals';
import {Observable} from 'rxjs/Rx';
import { UserService } from '../../../services/user.service';
import { UtilService } from '../../../services/util.service';
import { DataService } from '../../../services/data.service';

/*INSTALL*/
import swal from 'sweetalert2';

@Component({
	selector: 'app-list-professionalMap',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.css']
})
export class ListProfessionalMapComponent implements OnInit {

	review = {rating:4}
	professionals = [];
	professions = [];
        specialities = [];
	textSearch: String;
	order: String;
	location: String;
        pros: String;
        spec: String;
	rangeValues: number[] = [15,45];
	//lat: number = 51.678418;
	//lng: number = 7.809007;
	lat: number = 0;
	lng: number = 0;
	longUser: String;
        latUser: String;
	defaultImage:string='/assets/images/default.png';

	constructor(private userService: UserService, private utilService: UtilService, private dataService: DataService){

		this.textSearch  = "";
		this.order = "0";
		this.location = "0";
                this.pros = "0";
                this.spec = "0";
		this.longUser = "0";
		this.latUser = "0";

	}

	ngOnInit() {
		this.utilService.blockUiStart();
		this.userService.getUserLocation().subscribe(data => {
                	this.latUser = data.latitude;
                        this.longUser = data.longitude;
                	this.userService.getAllProfessionalsMap(this.longUser, this.latUser).subscribe(data => {
                        	this.professionals = data;
				this.lat = Number(this.latUser);
				this.lng = Number(this.longUser);
                        	this.dataService.getProfessions().subscribe(data => {
                                	this.professions = data;
                        	});
                        	this.dataService.getSpecialities().subscribe(data => {
                                	this.specialities = data;
                       		});
				this.utilService.blockUiStop();
                	});
                });

	}

	openNav() {
                $('.sidebar').addClass('expanded');
        }
        closeNav() {
                $('.sidebar').removeClass('expanded');
        }

	mobileSearch() {
                $('body').addClass('modal-open');
                $('#searchModal').addClass('in');
                $('#searchModal').css('display', 'block');
                $('body').append("<div class='modal-backdrop fade in'></div>");

        }
        mobileCloseSearch() {

                $('body').removeClass('modal-open');
                $('#searchModal').removeClass('in');
                $('#searchModal').css('display', 'none');
                $('.modal-backdrop').remove();


        }

	onSubmit($value){
		this.utilService.blockUiStart();
                this.userService.getMapLocationProfessionals(this.textSearch).subscribe(data => {
			this.lat = data.latitude;
			this.lng = data.longitude;
            		this.utilService.blockUiStop();
		});
        }

	public onFilters($value){
                this.utilService.blockUiStart();
                this.userService.getProfessionals(this.pros, this.spec, this.location, this.order, "0", "0", this.longUser, this.latUser).subscribe(data => {
                        this.professionals = data;
            		this.utilService.blockUiStop();
                });
	}

	
}
