import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate() {
        if (localStorage.getItem('token')) {
          let token = localStorage.getItem("token");
          //console.log(tokenNotExpired(null, token));
          if(!tokenNotExpired(null, token)){
            localStorage.clear();
            this.router.navigate(['/login']);
          }
          return tokenNotExpired(null, token);
        }

        // not logged in so redirect to login page
        this.router.navigate(['/login']);
        return false;
    }
}
