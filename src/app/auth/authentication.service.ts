import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import * as globals from '../../app/globals';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {
    public token: string;

    constructor(private http: Http, private router: Router) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    validateSession(){
      if (localStorage.getItem('currentUser'))
        this.router.navigate(['/']);
      else
        this.router.navigate(['/login']);
    }

    login(username: string, password: string): Observable<boolean> {
        return this.http.post(globals.baseUrl + '/login', `email=${username}&password=${password}`,
        {headers: this.getHeaderLogin()}).map((response: Response) => {
            let token = response.json() && response.json().token;
            if (token) {
                this.token = token;
                localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
                return true;
            } else {
                return false;
            }
        });
    }

    private getHeaderLogin() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return headers;
      }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}
