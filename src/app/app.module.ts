import * as firebase from 'firebase';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core'
import { StarRatingModule } from 'angular-star-rating';
import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from '../pages/home/home.component';
import { AboutComponent } from '../pages/about/about.component';
import { LoginComponent } from '../pages/login/login.component';
import { RegisterComponent } from '../pages/register/register.component';
import { ProfessionalRegisterComponent } from '../pages/professional-register/professional-register.component';
import { LogoutComponent } from '../pages/login/logout.component';
import { ForgotComponent } from '../pages/forgot/forgot.component';
import { PrivacyComponent } from '../pages/privacy/privacy.component';
import { FAQComponent } from '../pages/faq/faq.component';
import { MessagesComponent } from '../pages/messages/messages.component';
import { MessagesDeletedComponent } from '../pages/messages-deleted/messages-deleted.component';
import { MessagesSentComponent } from '../pages/messages-sent/messages-sent.component';
import { BlogComponent } from '../pages/blog/blog.component';
import { PostComponent } from '../pages/post/post.component';
import { ResetPasswordComponent } from '../pages/reset-password/reset-password.component';
import { MessageComponent } from '../pages/message/message.component';
import { ThreadComponent } from '../pages/thread/thread.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { Ng2CompleterModule } from "ng2-completer";

import { AgmCoreModule } from '@agm/core';

import { FooterComponent } from '../shared/general/footer/footer.component';
import { ImagesCategoryComponent } from '../shared/general/images-category/images-category.component';
import { NavbarComponent } from '../shared/general/navbar/navbar.component';
import { NavbarMapComponent } from '../shared/general/navbar-map/navbar.component';
import { NavbarListComponent } from '../shared/general/navbar-list/navbar.component';
import { ListProfessionalComponent } from '../shared/search/list-professional/list.component';
import { ListProfessionalMapComponent } from '../shared/search/list-professional-map/list.component';
import { ListComponent } from '../pages/list/list.component';
import { SearchComponent } from '../pages/search/search.component';
import { ListPatientComponent } from '../shared/search/list-patients/list.component';
import { ListPatientMapComponent } from '../shared/search/list-patients-map/list.component';

import { ResultsPatientComponent } from '../shared/results/results-patients/results.component';
import { ResultsProfessionalComponent } from '../shared/results/results-professional/results.component';

import { AuthGuard, AuthenticationService } from './auth/index';
import { ProfileHeaderComponent } from '../shared/user/profile-header/profile-header.component';
import { ProfileComponent } from '../pages/profile/profile.component';
import { ProfileTabsComponent } from '../shared/user/profile-tabs/profile-tabs.component';
import { ProfileTabsProfessionalComponent } from '../shared/user/profile-tabs-professional/profile-tabs.component';
import { JobDatailComponent } from '../shared/job/job-datail/job-datail.component';
import { FigureComponent } from '../shared/general/figure-image/figure-image.component'
import { PublicProfessionalComponent } from '../shared/user/public-professional/public.component';
import { PublicPatientComponent } from '../shared/user/public-patient/public.component';
import { UserComponent } from '../pages/user/user.component';

/*SERVICES*/
import { UserService  } from '../services/user.service';
import { UtilService  } from '../services/util.service';
import { PostJobService  } from '../services/post-job.service';
import { UploadFileService } from '../services/upload-file.service';
import { RegisterService } from '../services/register.service';
import { LocationService } from '../services/location.service';
import { DataService } from '../services/data.service';

/*INSTALL*/
import { BlockUIModule } from 'ng-block-ui';
import { MyDatePickerModule } from 'mydatepicker';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { Select2Module } from 'ng2-select2';
import {SliderModule} from 'primeng/slider';
import {RadioButtonModule} from 'primeng/radiobutton';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import {environment} from '../environments/environment';
import { ValidationMessagesModule } from 'ng2-custom-validation';
import { ContenteditableModule } from 'ng-contenteditable';
import { TabModule } from 'angular-tabs-component';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

export const fbConfig = {
	apiKey: "AIzaSyB-HNsmjdjWu5MN5fAzP6id1kIVWh4dH74",
    	authDomain: "paindown-770af.firebaseapp.com",
    	databaseURL: "https://paindown-770af.firebaseio.com",
    	projectId: "paindown-770af",
    	storageBucket: "paindown-770af.appspot.com",
    	messagingSenderId: "357958398702"
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    LoginComponent,
    FooterComponent,
    ImagesCategoryComponent,
    NavbarComponent,
    ProfileHeaderComponent,
    ProfileComponent,
    ProfileTabsComponent,
    ProfileTabsProfessionalComponent,
    JobDatailComponent,
    FigureComponent,
    RegisterComponent,
    ProfessionalRegisterComponent,
    UserComponent,
    PublicProfessionalComponent,
    PublicPatientComponent,
    NavbarMapComponent,
    NavbarListComponent,
    ListProfessionalComponent,
    ListProfessionalMapComponent,
    ListComponent,
	SearchComponent,
    ListPatientComponent,
    ListPatientMapComponent,
    LogoutComponent,
    ForgotComponent,
    PrivacyComponent,
    FAQComponent,
    MessagesComponent,
    MessagesDeletedComponent,
    MessagesSentComponent,
    BlogComponent,
    PostComponent,
    ResetPasswordComponent,
    MessageComponent,
    ThreadComponent,
	ResultsPatientComponent,
	ResultsProfessionalComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'Laboriosos'}),
    AppRoutingModule,
    AutocompleteModule,
    ContenteditableModule,
    Ng2CompleterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BlockUIModule,
    MyDatePickerModule,
    NguiAutoCompleteModule,
    AngularFireModule.initializeApp(environment.firebase, 'paindown'),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ValidationMessagesModule.forRoot(),
    Select2Module,
    StarRatingModule.forRoot(),
    SliderModule,
    RadioButtonModule,
    TabModule,
    AngularDualListBoxModule,
    PerfectScrollbarModule,
    AccordionModule.forRoot(),
    AgmCoreModule.forRoot({
    	apiKey: 'AIzaSyBn58_p7Z-kdIRleiC8Hug_lnMSq4sn7Pw'
    })
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    UserService,
    UtilService,
    PostJobService,
    UploadFileService,
    RegisterService,
    LocationService,
    DataService,
    {
    	provide: PERFECT_SCROLLBAR_CONFIG,
	useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
export class AppBootstrapModule {}
