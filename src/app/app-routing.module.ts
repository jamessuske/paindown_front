import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../pages/home/home.component';
import { AboutComponent } from '../pages/about/about.component';
import { AuthGuard } from './auth/index';

import { LoginComponent } from '../pages/login/login.component';
import { LogoutComponent } from '../pages/login/logout.component';
import { ForgotComponent } from '../pages/forgot/forgot.component';

import { ProfileHeaderComponent } from '../shared/user/profile-header/profile-header.component';
import { ProfileComponent } from '../pages/profile/profile.component';
import { RegisterComponent } from '../pages/register/register.component';
import { ProfessionalRegisterComponent } from '../pages/professional-register/professional-register.component';
import { UserComponent } from '../pages/user/user.component';
import { ListComponent } from '../pages/list/list.component';
import { SearchComponent } from '../pages/search/search.component';
import { PrivacyComponent } from '../pages/privacy/privacy.component';
import { FAQComponent } from '../pages/faq/faq.component';
import { BlogComponent } from '../pages/blog/blog.component';
import { PostComponent } from '../pages/post/post.component';
import { MessagesComponent } from '../pages/messages/messages.component';
import { MessagesDeletedComponent } from '../pages/messages-deleted/messages-deleted.component';
import { MessagesSentComponent } from '../pages/messages-sent/messages-sent.component';
import { ResetPasswordComponent } from '../pages/reset-password/reset-password.component';
import { MessageComponent } from '../pages/message/message.component';
import { ThreadComponent } from '../pages/thread/thread.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
  	path: 'about',
	component: AboutComponent
  },
  {
  	path: 'message/:username',
	component: MessageComponent
  },
  {
  	path: 'thread/:id',
	component: ThreadComponent
  },
  {
  path: 'reset-password/:token/:email',
	component: ResetPasswordComponent
  },
  {
  	path: 'privacy',
	component: PrivacyComponent
  },
  {
  	path: 'faq',
	component: FAQComponent
  },
  {
  	path: 'blog',
	component: BlogComponent
  },
  {
  	path: 'post/:id',
	component: PostComponent
  },
  {
  	path: 'messages',
	component: MessagesComponent,
    	canActivate: [AuthGuard]
  },
  {
  	path: 'messages-deleted',
	component: MessagesDeletedComponent,
    	canActivate: [AuthGuard]
  },
  {
  	path: 'messages-sent',
	component: MessagesSentComponent,
    	canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'account',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'professional-register',
    component: ProfessionalRegisterComponent
  },
  {
    path: 'user/:username',
    component: UserComponent
  },
  {
    path: 'search/:type',
    component: ListComponent
  },
  {
    path: 'results/:type/:order/:location/:long/:lat/:minRangeDistance/:maxRangeDistance/:minRangePain/:maxRangePain/:painType/:painkillers/:pros/:spec',
    component: SearchComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
  	path: 'forgot',
	component: ForgotComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
